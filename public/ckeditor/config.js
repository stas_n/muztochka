CKEDITOR.editorConfig = function( config ) {
    config.toolbar = 'Basic';

    config.toolbar_Full =
        [
            { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
                'HiddenField' ] },

            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
                '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
            '/',
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
        ];

    config.toolbar_Basic =
        [
            {name: 'styles', items:["Bold","Italic"]},
            { name: 'paragraph', items: [ "NumberedList", "BulletedList", "Outdent", "Indent"] },
            { name: 'links', items: [ 'Link','Unlink' ] },
            '/',
            {name:'text_format',items:["Format","Font","FontSize"]},
            {name:'text_color',items:["TextColor"]},
            { name: 'tools', items: [ 'Maximize','-' ] },

            format_tags = 'h1;h2;h3',
            removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Redo,Undo,Find,Scayt,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Superscript,Subscript,Strike,Underline,RemoveFormat,Blockquote,CreateDiv,JustifyCenter,JustifyLeft,JustifyBlock,JustifyRight,Language,BidiRtl,BidiLtr,Anchor,Flash,Image,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,BGColor,Styles,ShowBlocks,About,Replace'

    ];


    config.toolbar_Message =
        [
            {name: 'styles', items:["Bold","Italic"]},
            { name: 'paragraph', items: [ "NumberedList", "BulletedList", "Outdent", "Indent"] },
            { name: 'links', items: [ 'Link','Unlink' ] },
          

            format_tags = 'h1;h2;h3',
            removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Redo,Undo,Find,Scayt,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Superscript,Subscript,Strike,Underline,RemoveFormat,Blockquote,CreateDiv,JustifyCenter,JustifyLeft,JustifyBlock,JustifyRight,Language,BidiRtl,BidiLtr,Anchor,Flash,Image,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,BGColor,Styles,ShowBlocks,About,Replace'

        ];

};