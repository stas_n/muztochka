
require 'nokogiri'
@builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
  xml.root {
    xml.date Time.now.iso8601
    xml.shop{
      xml.name 'My shop'
      xml.company 'My company'
      xml.url 'https://mysite.com'
    }
    xml.categories{
      Category.all.each do |c|
        xml.category(c.title, id: c.id)
      end
    }
    xml.subcategories{
      Subcategory.all.order('id').each do |c|
        xml.subcategory(c.title, id: c.id,'parentId':c.category.id)
      end
    }
    xml.details{
      Detail.all.order('id').each do |c|
        xml.detail(c.title, id: c.id,'parentId':c.category.id)
      end
    }
    xml.conditions{
      [["Новое", 0],
       ["Как новое", 1],
       ["Отличное", 2],
       ["Очень хорошее", 3],
       ["Хорошее", 4],
       ["Удовлетворительное", 5],
       ["Плохое", 6],
       ["Нерабочее", 7]].each do |c|
        xml.condition(c[0],id:c[1])
      end
    }
    xml.products {
      Product.all.offset(520).first(2).each do |p|
        xml.product {
          xml.description p.description
          xml.images{
            p.photos.each do|ph|
              xml.image ph.image.url
            end
          }
          xml.condition 0
          xml.brand p.brand.try :title
          xml.mark p.mark
          xml.price p.price.to_i
          xml.city p.city
          xml.category 1
          xml.subcategory 1
          xml.detail 1
          xml.video p.video
          xml.country_producing p.country_producing
          xml.year p.year
          xml.color p.color
          xml.handmade  p.handmade
        }
      end
    }
  }
end
respond_to do |format|
  format.xml { render :xml => @builder.to_xml }


end
send_data @builder.to_xml,
          :type => 'text/xml',
          :disposition => 'attachment; filename=products.xml'