class XmlConnection
  class << self

    def frontman
      request = HTTParty.get('http://frontman.kh.ua/yandex_market.xml?hash_tag=536534f5527915a957386a86fb40af73&group_ids=9036880%2C9036895%2C9036900%2C9095487%2C9095519%2C9096106%2C9096290%2C9584763&exclude_fields=&sales_notes=&product_ids=')
      handle_errors(request)
    end

    def guitarmania
      request = HTTParty.get('http://www.guitaramania.ru/bitrix/catalog_export/yandex_334621.php')
      handle_errors(request)
    end


    def handle_errors(request)
      if request.code == 200
        request
      else
        raise 'Connection error'
      end
    end

  end
end

