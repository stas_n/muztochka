class Frontman


  def initialize
    @request = XmlConnection.frontman
    @categories = {}
  end


  def products
    @request['yml_catalog']['shop']['offers']['offer'].sort_by{|p|p['id']}
  end

  def categories
    cat = @request['yml_catalog']['shop']['categories']['category']
    cat.map { |x| x.symbolize_keys }
  end


   def csv
    CSV.read(Rails.root.to_s + '/lib/csv/frontman.csv', headers: true, header_converters: :symbol, converters: :all).collect do |row|
      Hash[row.collect { |c, r| [c, r] }]
    end
  end


  def set_category(id)
    title = csv.select { |key, hash| key[:frontman_category_id]==id.to_i }[0][:muzdot_cat]
    Category.find_by_title(title)
  end

  def set_subcat(id)
    title = csv.select { |key| key[:frontman_category_id] == id.to_i }[0][:muzdot_subcat]
    subcat = Subcategory.find_by_title(title)
    category = set_category(id)
    subcat.nil? ? category.subcategories.first : subcat
  end

  def set_brand(vendor)
    Brand.find_by(title: vendor).try(:id)
  end

  def set_detail(id)
    title = csv.select { |key| key[:frontman_category_id] == id.to_i }[0][:muzdot_detail]
    category_id = set_category(id).try(:id)
    Detail.find_by(category_id: category_id, title: title).try(:id)
  end


  def get_index(id)
    el = products.select{|p|p['id'] == id.to_s}.first
    products.index(el)
  end


  def rand_time(from, to=Time.now)
    Time.at(rand_in_range(from.to_f, to.to_f))
  end

  def rand_in_range(from, to)
    rand * (to - from) + from
  end

end
