class Bigband


  def big_band
    xml = File.open(Rails.root.to_s+'/lib/xml/big_band.xml')
    Hash.from_xml(xml)
  end

  def products
    big_band['root']['products']['product']
  end

  def possible_brands
    big_band['products']['product'].map{|x|x['name']}.map{|x|x.split(' ')}.sort_by {|x| x.length}.map{|x|x[-2]}.uniq
  end

  def check_if_new_brands
    possible_brands.each{|b|Brand.find_or_create_by(title:b)}
  end

  def set_mark(el)
    el['name'].split(' ').last
  end


  def set_category(category)
    Category.find(category) rescue Category.find_by(title:'Прочее')
  end

  def set_subcat(subcategory)
    Subcategory.find(subcategory) rescue Subcategory.find_by(title:'Прочее')
  end

  def set_brand(vendor)
    Brand.find_by(title: vendor).try(:id)
  end

  def set_description(p)
    p['description'].nil? ? p['name'] : p['description']
  end

  def rand_time(from, to=Time.now)
    Time.at(rand_in_range(from.to_f, to.to_f))
  end

  def rand_in_range(from, to)
    rand * (to - from) + from
  end
end

