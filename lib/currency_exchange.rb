module CurrencyExchange

  CURRENCIES = %w(uah rub eur usd)

  def request_rates
    p 'rates requested'
    rate = HTTParty.get('http://api.minfin.com.ua/nbu/aca0f2bcfc5a454366e82c0baa8657d7d8a45a06')
    Rails.cache.write('rate',JSON.parse(rate.body))
  end

  def respond_rates
    Rails.cache.fetch('rate')
  end

  def rates
    respond_rates.values.map{|x|x.except('date','trendAsk','trendBid')}
  end

  def rates_with_uah
    arr = rates.map { |x| [x['currency'], x['ask'], x['bid']] }
    uah = []
    arr[0..2].each { |x| uah << {
                                 'ask' => nbu_format(x[1]),
                                 'bid' => nbu_format(x[2]),
                                 'currency' => 'uah',
                                 'per_one' =>x[0] }}
    uah + rates
  end

  def get_rates(currency = 'usd')
    if all_ok?(currency)
      rates_with_uah.select { |x| x['currency'] == currency }
    else
      invalid_currency
    end
  end

  def todays_rate(currency = 'usd')
    value = get_rates(currency).first['ask']
    ceiling(value)
  end

  def to_app_price(price, currency = 'usd') #for xml
    if currency == 'usd'
      price
    else
      send("#{currency.downcase}_to_usd", price.to_f)
    end
  end


  def get_uah(to_currency)
    return if to_currency == 'uah'
    value = get_rates('uah').select { |c| c['per_one'] == to_currency }.first['ask']
    ceiling(value)
  end

  def self.available_currencies
    CURRENCIES.join(' ').downcase.split.permutation(2)
  end

  available_currencies.each do |(c1, c2)|
    define_method(:"#{c1}_to_#{c2}") do |cr|
      check_uah_rate(cr, c1, c2)
    end
  end


  def check_uah_rate(cr, c1, c2)
    if c1 =='uah'
      currency = get_uah(c2)
      value = (cr * currency)
      ceiling(value)
    elsif c2 =='uah'
      currency = todays_rate(c1)
      value = (cr * currency)
      ceiling(value)
    else
      from = todays_rate(c1)
      to = todays_rate(c2)
      value = (cr * from / to)
      ceiling(value)
    end
  end


  def ceiling(val)
    val.to_d.round(3, :truncate).to_f #'24.89808'=>24.898
  end

  def all_ok?(currency)
    CURRENCIES.include?(currency)
  end

  def nbu_format(x)
    (1.to_f/x.to_f).round(5).to_s
  end

  def invalid_currency
    raise ArgumentError, "Available currencies are #{CURRENCIES.join(',')}"
  end

end
