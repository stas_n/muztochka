namespace :db do
  desc 'reseed db'
  task :reseed => [:remake, :seed]
end
