namespace :db do
  desc 'redefine permissions of product'
  task redefine_permissions_of_product: :environment do

    products = Product.unscoped.where(status: false).to_a
    puts "found #{products.count} not active products"
    if products.present?
      products.each do |product|
        if product.status == false
          product.permission  = 'non_active'
          product.status = false
          if product.save(validate: false)
            puts "product #{product.id} updated"
          else
            puts "product with id #{product.id} not updated"
          end
        end
      end
    else
      puts 'Nothing to update'
    end

  end
end
