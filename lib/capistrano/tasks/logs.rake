namespace :logs do
  desc "tail logs"

  task :app do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/#{fetch(:rails_env)}.log"
    end
  end

  task :puma do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/puma.*.log"
    end
  end
end

