task :xml_frontman do
  on roles(:db) do
    within release_path do
      with rails_env: fetch(:rails_env) do
       XmlWorker.perform_async
      end
    end
  end
end