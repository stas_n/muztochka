namespace :deploy do
  namespace :db do
    task :setup do
      on roles(:db) do
        ask(:sure, "Are you sure to wipe the entire database (anything other than 'yes' aborts):")
        raise RuntimeError.new('setup_database aborted!') unless fetch(:sure) == 'yes'

        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:drop', 'db:create'
          end
        end
      end
    end

    task :seed do
      on roles(:db) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:seed'
          end
        end
      end
    end

    task :sample do
      on roles(:db) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:sample'
          end
        end
      end
    end
  end
end
