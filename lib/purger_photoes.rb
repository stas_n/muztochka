class PurgerPhotoes
  def self.call
    photos = Photo.where('product_id = ? AND post_id = ?', nil, nil)
    photos.destroy_all if photos.any?
  end
end