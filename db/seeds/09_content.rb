print 'Content.......'

contents = Content.create([{
  title: 'home',
  seo_title: 'Главная',
  seo_description: ''
}, {
  title: 'brands',
  seo_title: 'Бренды',
  seo_description: ''  
}, {
  title: 'about',
  seo_title: 'О нас',
  seo_description: ''
}, {
  title: 'countries',
  seo_title: 'Страны',
  seo_description: ''
}, {
  title: 'cities',
  seo_title: 'Города',
  seo_description: ''
}, {
  title: 'faq',
  seo_title: 'FAQ',
  seo_description: ''
}, {
  title: 'contacts',
  seo_title: 'Контакты',
  seo_description: ''
}, {
  title: 'partners',
  seo_title: 'Партнеры',
  seo_description: ''
}, {
  title: 'why_muzpoint',
  seo_title: 'Почему музточка?',
  seo_description: ''
}, {
  title: 'how_advertising_works',
  seo_title: 'Как работает реклама',
  seo_description: ''
}, {
  title: 'condition',
  seo_title: 'Состояние',
  seo_description: ''   
}, {
  title: 'terms_of_use',
  seo_title: 'Пользоватеьское соглашение',
  seo_description: ''    
}])

puts "OK \n\n"
