print 'Categories.......'

categories_titles = ['Электрогитары', 'Педали и эффекты', 'Усилители гитарные', 'Акустические гитары',
                      'Аксессуары и детали', 'Бас-гитары', 'Ударные и перкуссия',
                      'Духовые', 'Смычковые', 'Щипковые', 'Народные', 'Клавишные',
                      'DJ оборудование', 'Звук', 'Свет и шоу', 'Прочее']

categories = (0..15).to_a.map do |i|
  Category.create({
    title: categories_titles[i % categories_titles.length],
    position: i+1
    })
end

puts "*** OK *********\n\n"
