class AddIndexes < ActiveRecord::Migration
  def change
    add_index :brands, :slug

    add_index :categories, :slug

    add_index :cities, :country_id

    add_index :details, :category_id

    add_index :favorites, :user_id
    add_index :favorites, :product_id

    add_index :photos, :product_id
    add_index :photos, :post_id

    add_index :posts, :slug
    add_index :posts, :category

    add_index :posts_photos, :post_id

    add_index :products, :permission
    add_index :products, :category_id
    add_index :products, :subcategory_id
    add_index :products, :detail_id
    add_index :products, :brand_id
    add_index :products, :shop_id
    add_index :products, :slug

    add_index :promo_items, :promo_service_id
    add_index :promo_items, :product_id

    add_index :shops, :user_id
    add_index :shops, :slug

    add_index :social_accounts, :user_id

    add_index :subcategories, :category_id
    add_index :subcategories, :slug
  end
end
