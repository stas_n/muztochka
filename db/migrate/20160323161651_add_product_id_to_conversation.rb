class AddProductIdToConversation < ActiveRecord::Migration
  def change
    add_column :conversations, :product_id, :integer
  end
end
