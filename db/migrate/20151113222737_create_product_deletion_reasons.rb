class CreateProductDeletionReasons < ActiveRecord::Migration
  def change
    create_table :product_deletion_reasons do |t|
      t.string :name_ru, null: false
      t.boolean :is_active, default: false, null: false

      t.timestamps
    end
  end
end
