class AddSeoKeywords < ActiveRecord::Migration
  def change
    add_column :categories,    :seo_keywords, :text
    add_column :subcategories, :seo_keywords, :text
    add_column :products,      :seo_keywords, :text
    add_column :contents,      :seo_keywords, :text
    add_column :brands,        :seo_keywords, :text
  end
end
