class AddFieldsToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :show_for_sender, :boolean, default: true
    add_column :conversations, :show_for_recipient, :boolean, default: true
  end
end
