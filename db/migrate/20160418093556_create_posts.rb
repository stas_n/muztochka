class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :author
      t.text :article
      t.string :video
      t.string :slug
      t.integer :post_category_id
      t.boolean :important, :default => false
      t.timestamps
    end
  end
end
