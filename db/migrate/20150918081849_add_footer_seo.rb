class AddFooterSeo < ActiveRecord::Migration
  def change
    add_column :categories, :seo_text, :text

    add_column :subcategories, :seo_text, :text

  end
end
