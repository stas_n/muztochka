class NewPhotoStylesForFotorama < ActiveRecord::Migration
  Photo.find_each(batch_size: 500) do |photo|
    photo.image.reprocess!(:fotorama)
    photo.image.reprocess!(:fotorama_thumb)
  end
  p 'Done...'
end
