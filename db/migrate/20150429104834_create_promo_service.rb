class CreatePromoService < ActiveRecord::Migration
  def change
    create_table :promo_services do |t|
    	t.string :title
    	t.integer :view_count
      t.decimal :price, precision: 8, scale: 3
    	
    	t.timestamps
    end
  end
end
