class AddCountToDetailsAndSubcategories < ActiveRecord::Migration
  def change
    add_column :details, :products_count, :integer, null: false, default: 0
    add_column :subcategories, :products_count ,:integer, null: false,  default: 0


    Detail.reset_column_information
    Subcategory.reset_column_information

    Detail.all.each do |d|
      Detail.update_counters d.id, products_count: d.products.length
    end

    Subcategory.all.each do |s|
      Subcategory.update_counters s.id, products_count: s.products.length
    end

  end
end
