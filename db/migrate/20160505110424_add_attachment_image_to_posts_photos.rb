class AddAttachmentImageToPostsPhotos < ActiveRecord::Migration
  def self.up
    change_table :posts_photos do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :posts_photos, :image
  end
end
