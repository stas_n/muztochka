class UpdateProductKyiv < ActiveRecord::Migration
  def change
    city = "UPDATE products SET city = REPLACE(city, 'Київ', 'Киев');"
    execute city

    formatted_address = "UPDATE products SET formatted_address = REPLACE(formatted_address, 'Київ', 'Киев');"
    execute formatted_address
  end
end
