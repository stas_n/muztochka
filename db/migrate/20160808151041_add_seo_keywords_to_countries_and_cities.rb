class AddSeoKeywordsToCountriesAndCities < ActiveRecord::Migration
  def change
    add_column :countries, :seo_keywords, :text
    add_column :cities, :seo_keywords, :text

  end
end
