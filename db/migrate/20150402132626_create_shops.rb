class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :title
      t.text :description
      t.attachment :cover
      t.string :site
      t.string :delivery_place
      t.text :delivery_method
      t.text :return

      t.string :slug

      t.string :formatted_address
      t.string :country
      t.string :state
      t.string :city

      t.float :lat
      t.float :lng

      t.references :user

      t.timestamps
    end
  end
end
