class AddIsReuploadToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_reupload, :boolean, default:false, nil: false
  end
end
