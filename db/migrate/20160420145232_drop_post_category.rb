class DropPostCategory < ActiveRecord::Migration
  def change
    drop_table :post_categories
    remove_column :posts, :post_category_id
  end
end
