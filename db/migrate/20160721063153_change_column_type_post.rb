class ChangeColumnTypePost < ActiveRecord::Migration
  def change
    change_column :posts, :first_paragraph_title,  :text
  end
end
