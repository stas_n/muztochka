class CreatePromoItem < ActiveRecord::Migration
  def change
    create_table :promo_items do |t|
    	t.decimal :price, precision: 8, scale: 3
    	t.references :promo_service
    	t.references :product
    	t.integer :view_count
    	t.string :status

    	t.timestamps
    end
  end
end
