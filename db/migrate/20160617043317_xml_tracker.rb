class XmlTracker < ActiveRecord::Migration
  def change
    create_table :xml_trackers do |t|
      t.boolean :is_saved, default:false
      t.integer :xml_id
    end
  end
end
