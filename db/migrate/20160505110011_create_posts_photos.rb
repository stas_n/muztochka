class CreatePostsPhotos < ActiveRecord::Migration
  def change
    create_table :posts_photos do |t|
      t.string :image
      t.integer :post_id

      t.timestamps
    end
  end
end
