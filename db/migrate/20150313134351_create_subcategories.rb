class CreateSubcategories < ActiveRecord::Migration
  def change
    create_table :subcategories do |t|
      t.string :title
      t.integer :position
      t.references :category

      t.timestamps
    end
  end
end
