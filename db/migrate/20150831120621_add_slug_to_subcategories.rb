class AddSlugToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :slug, :string

    add_column :subcategories, :text, :text

    add_column :subcategories, :image_file_name, :string
    add_column :subcategories, :image_content_type, :string
    add_column :subcategories, :image_file_size, :integer
    add_column :subcategories, :image_updated_at, :datetime
  end
end
