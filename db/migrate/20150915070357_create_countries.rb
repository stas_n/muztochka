class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.string :permalink

      t.text :text
      t.text :seo_text
      t.attachment :image

      t.string :seo_title
      t.text :seo_description

      t.timestamps
    end
  end
end
