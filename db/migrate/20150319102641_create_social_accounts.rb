class CreateSocialAccounts < ActiveRecord::Migration
  def change
    create_table :social_accounts do |t|
      t.string :provider
      t.string :uid
      t.references :user

      t.timestamps
    end
  end
end
