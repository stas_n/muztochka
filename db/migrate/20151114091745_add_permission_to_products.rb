class AddPermissionToProducts < ActiveRecord::Migration
  def change
    add_column :products, :permission, :integer, default: 0, null: false
  end
end
