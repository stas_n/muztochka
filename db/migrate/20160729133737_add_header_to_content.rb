class AddHeaderToContent < ActiveRecord::Migration
  def change
    add_column :contents, :header, :string
  end
end
