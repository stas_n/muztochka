class CreateVisitorProducts < ActiveRecord::Migration
  def change
    create_table :visitor_products do |t|
      t.references :product, index: true
      t.string :remote_ip

      t.timestamps
    end
  end
end
