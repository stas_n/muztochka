class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.string :state

      t.string :permalink
      t.string :permalink_state

      t.text :text
      t.text :seo_text

      t.attachment :image
      t.references :country

      t.string :seo_title
      t.text :seo_description

      t.timestamps
    end
  end
end
