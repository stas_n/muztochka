class AddColumnsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :first_paragraph_title, :string
    add_column :posts, :first_paragraph_text, :text
    add_column :posts, :second_paragraph_title, :string
    add_column :posts, :second_paragraph_text, :text

  end
end
