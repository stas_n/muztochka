puts '*** Seeding ****'

Dir[Rails.root.join('db/seeds/*.rb')].sort.each { |f| require f }

puts "*** OK *********\n\n"
