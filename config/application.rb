require File.expand_path('../boot', __FILE__)

require 'rails/all'

require  './lib/currency_exchange'
include CurrencyExchange

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MuzDot
  class Application < Rails::Application
    config.exceptions_app = self.routes

    config.i18n.default_locale = :ru
    I18n.enforce_available_locales = false
    # config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    # config.assets.precompile += %w( ckeditor/* )

    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += Dir["#{config.root}/lib/**/"]

    config.time_zone = 'Kyiv'
    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get]
      end
    end
    config.after_initialize do
      request_rates if respond_rates.blank? && !Rails.env.test?
    end
  end
end
