MetaTags.configure do |c|
  c.title_limit        = 120
  c.description_limit  = 210
  c.keywords_limit     = 255
  c.keywords_separator = ', '
end
