Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks",
                                       :confirmations => "confirmations",
                                       :passwords => "passwords",
                                       :sessions => "sessions" }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount Ckeditor::Engine => '/ckeditor'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/admin/sidekiq'
  get '/triggers_test/', :to => 'triggers_test#triggers_test'

  root 'home#index'
  resources :brands
  resources :users, except: [:destroy, :index] do
    member do
      get :favorites
      get :goods
      patch :set_password
      delete :destroy_social_account
      delete :delete_avatar
    end
  end
  resources :products, except: [:index] do
    collection do
      post 'get_subcategories'
      post 'get_details'
      get 'fake_typing_titles'
      get 'dynamic_admin_subcategory'
    end
  end

  resources :categories, only: [:show]

  resources :photos, only: [:create, :destroy]
  resources :favorites, only: [:create, :destroy]

  resources :shops, :controller => :shops do
    collection do
      post :update_shop_cover
      get :delete_shop_cover
    end
  end

  resources :promo_items, only: [:create, :update, :show] do
    member do
      get 'notify'
    end
  end

  controller :payments do
    post 'notify', as: :notify
  end

  post '/liqpay_payment' => 'payments#liqpay_payment'

  resources :conversations, only: [:index, :show, :create, :destroy]

  controller :static do
    get 'about', as: :about
    get 'condition', as: :condition
    get 'faq', as: :faq
    get 'contacts', as: :contacts
    get 'why_muzpoint', as: :why_muzpoint
    get 'how_advertising_works', as: :how_advertising_works
    get 'terms_of_use', as: :terms_of_use
  end

  resources :subscribers, :only => [:create]
  resources :feedbacks, :only => [:create]
  post 'mark_as_read',to:'messages#mark_as_read'
  resources :posts,path: :blog do
    collection do
      get 'articles'
      get 'photos'
      get 'overviews'
      get 'lessons'
    end
  end
  devise_scope :user do
    post 'session_path',to: 'sessions#path'
  end
  post '/switch_currency',to: 'switch_currency#switch'

  get 'template',to: 'xml_templates#template'
  get '/countries', to: 'countries#index', as: :countries, controller: "countries"
  get '/countries/:id', to: 'countries#show', as: :country, controller: "countries"

  get '/cities', to: 'cities#index', as: :cities, controller: "cities"
  get '/countries/:country_id/:state_id/:id', to: 'cities#show', as: :city, controller: "cities"

  get '/categories/:category_id/:id', to: 'subcategories#show', as: :subcategory, controller: "subcategories"

  get '404', :to => 'errors#page_not_found'
  get '500', :to => 'errors#server_error'
  get '/:category_id/:id', to: redirect("/categories/%{category_id}/%{id}", status: 301)
end
