#!/bin/bash

cp -f --no-preserve=all /home/muz/app/current/config/custom/nginx/stage_main.conf        /etc/nginx/conf.d/muztochka.conf
cp -f --no-preserve=all /home/muz/app/current/config/custom/nginx/stage_mobile.conf      /etc/nginx/conf.d/muztochka_mobile.conf
