#!/bin/bash

cp -f --no-preserve=all /home/muz/app/current/config/custom/nginx/prod_main.conf        /etc/nginx/conf.d/muztochka.conf
cp -f --no-preserve=all /home/muz/app/current/config/custom/nginx/prod_main_ssl.conf    /etc/nginx/conf.d/muztochka_ssl.conf

cp -f --no-preserve=all /home/muz/app/current/config/custom/nginx/prod_mobile.conf      /etc/nginx/conf.d/muztochka_mobile.conf
cp -f --no-preserve=all /home/muz/app/current/config/custom/nginx/prod_mobile_ssl.conf  /etc/nginx/conf.d/muztochka_mobile_ssl.conf
