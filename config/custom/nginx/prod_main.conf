proxy_cache_path /tmp/cache-muz keys_zone=cache-muz:10m levels=1:2 inactive=600s max_size=100m;

upstream muztochka-upstreams {
  zone muz   128k;
  keepalive  20;
  server unix:/home/muz/app/shared/tmp/sockets/puma.socket;
}

server {
    listen         80;
    server_name    www.muztochka.com;
    server_tokens  off;
    access_log     off;
    error_log      /dev/null crit;
    rewrite ^(.*)  http://muztochka.com$1 permanent;
}

server {
  listen         80;
  server_name    muztochka.com;
  server_tokens  off;
  access_log     /var/log/nginx/muz_main_access.log  main;
  error_log      /var/log/nginx/muz_main_error.log   info;

  root  /home/muz/app/current/public;
  rewrite ^/(.*)/$ /$1 permanent;

  proxy_cache cache-muz;
  proxy_cache_lock on;
  proxy_cache_valid 200 1s;
  proxy_cache_use_stale updating;

  location / {
    try_files $uri $uri/index.html $uri.html @muztochka;
  }

  location @muztochka {
    gzip off;

    proxy_read_timeout      300;
    proxy_connect_timeout   300;
    proxy_redirect          off;

    proxy_set_header    Host                $http_host;
    proxy_set_header    X-Real-IP           $remote_addr;
    proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
    proxy_set_header    X-Frame-Options     SAMEORIGIN;

    proxy_pass http://muztochka-upstreams;
  }

  location /websocket {
      gzip off;
      proxy_read_timeout      300;
      proxy_connect_timeout   300;
      proxy_redirect          off;

      proxy_pass http://muztochka-upstreams;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
  }

  # Return a 503 error if the maintenance page exists.
  if (-f /home/muz/app/shared/public/system/maintenance.html) {
    return 503;
  }

  error_page 503 @503;

  location @503 {
    # Serve static assets if found.
    if (-f $request_filename) {
      break;
    }

    # Set root to the shared directory.
    root /home/muz/app/shared/public;
    rewrite ^(.*)$ /system/maintenance.html break;
  }
}
