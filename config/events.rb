WebsocketRails::EventMap.describe do
  subscribe :new_message,
    :to => ChatWebsocketController, :with_method => :new_message
end
