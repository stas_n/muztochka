every 1.day, :at => '5:00 am' do
  rake "-s sitemap:refresh"
  runner 'PurgerPhotoes.call'
end


every 1.day, :at => '7:00 pm' do
  runner 'CurrencyExchange.request_rates'
end