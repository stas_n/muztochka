set :rails_env,        :staging
set :application,      'muz_dot'
set :bundle_jobs,      2
set :rvm_ruby_version, 'ruby-2.3.0@muz_dot'

server 'muzdot.thedigitalcrafters.com',
  user:   'muz',
  roles:  %w(app web db),
  ssh_options: {
    forward_agent:  true,
    port:           2000
  }

set :deploy_to, '/home/muz/app/'
