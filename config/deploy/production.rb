set :rails_env,        :production
set :application,      'muz_dot'
set :bundle_jobs,      6
set :rvm_ruby_version, 'ruby-2.3.0@muz_dot'

server 'guitar.muztochka.com',
  user:   'muz',
  roles:  %w(app web db),
  ssh_options: {
    forward_agent:  true,
    port:           2000
  }

set :deploy_to, '/home/muz/app/'

namespace :deploy do
  after :published, 'sitemap:refresh'
end

