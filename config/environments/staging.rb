Rails.application.configure do
  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  config.serve_static_assets = false
  config.assets.js_compressor = :uglifier
  config.assets.compile = true
  config.assets.digest = true
  config.assets.version = '1.0'
  config.force_ssl = false
  config.log_level = :info
  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.log_formatter = ::Logger::Formatter.new

  config.action_mailer.smtp_settings = {
    address:              'smtp.gmail.com',
    port:                 587,
    domain:               'gmail.com',
    user_name:            'message@muztochka.com',
    password:             'muzDS=y4Z44',
    authentication:       'plain',
    enable_starttls_auto: true
  }
  config.action_mailer.default_url_options = {:host => "muzdot.thedigitalcrafters.com", :protocol => :http }
  config.action_controller.asset_host = 'http://muzdot.thedigitalcrafters.com/'
  config.action_mailer.asset_host = 'http://muzdot.thedigitalcrafters.com/'

  config.session_store :cookie_store, key: '_stg_muz_dot_session', domain: 'muzdot.thedigitalcrafters.com'

  config.paperclip_defaults = {
    :storage => :s3,
    :region => 'eu-central-1',
    :s3_host_name => 's3-eu-central-1.amazonaws.com',
    :s3_protocol => :http,

    :s3_credentials => {
      :bucket => 'muztochkaproduction',
      :access_key_id => 'AKIAJ4I5QF57KZNMPYSQ',
      :secret_access_key => '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'
    }
  }
end
