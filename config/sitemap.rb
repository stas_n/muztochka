SitemapGenerator::Sitemap.default_host = Rails.application.config.action_controller.asset_host

SitemapGenerator::Sitemap.create(compress: (Rails.env != 'development')) do

  add brands_path
  add about_path
  add condition_path
  add faq_path
  add contacts_path
  add why_muzpoint_path
  add how_advertising_works_path
  add terms_of_use_path

  Shop.find_each do |shop|
    add shop_path(shop), lastmod: shop.updated_at
  end

  Brand.find_each do |brand|
    add brand_path(brand), lastmod: brand.updated_at
  end

  Product.find_each do |product|
    add product_path(product), lastmod: product.updated_at
  end

  Category.find_each do |category|
    add category_path(category), lastmod: category.updated_at
  end

  Subcategory.find_each do |subcategory|
    add subcategory_path(subcategory.category, subcategory), lastmod: subcategory.updated_at
  end

end
