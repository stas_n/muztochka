lock '3.4.0'

Airbrussh.configure do |config|
  config.command_output = true
end

set :repo_url,             'git@bitbucket.org:digitalcrafters/muz_dot.git'
set :application,          'muz_dot'
set :user,                 'deploy'
set :linked_dirs,          fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/assets')
set :linked_files,         fetch(:linked_files, []).push('config/database.yml', 'config/application.yml', 'config/newrelic.yml')
set :forward_agent,        true
set :whenever_identifier,  -> { "#{fetch(:application)}_#{fetch(:stage)}" }
set :pty,                  true
set :keep_releases,        15
ask :branch,               proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

namespace :deploy do
  task :start do
    on roles(:app) do
      execute '/etc/init.d/muztochka start'
    end
  end

  task :stop do
    on roles(:app) do
      execute '/etc/init.d/muztochka stop'
    end
  end

  task :restart do
    on roles(:app) do
      execute '/etc/init.d/muztochka stop'
      execute '/etc/init.d/muztochka start'
    end
  end

  before :publishing, 'maintenance:enable'

  after :published, 'robots:symlink'
  after :published, 'puma:symlink'
  after :published, 'deploy:restart'
  after :published, 'maintenance:disable'
end
