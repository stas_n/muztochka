require 'spec_helper'

describe FeedbacksController do
  render_views

  before(:all) do
    @feedback = create(:feedback)
  end

  describe 'create' do
    it 'new feedback' do
      xhr :post, :create, {feedback: @feedback.attributes}
      expect(response).to render_template('feedbacks/success')
    end
  end

end