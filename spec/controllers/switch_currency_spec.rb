require 'spec_helper'

describe SwitchCurrencyController do
  render_views
  before do
    request.env["HTTP_REFERER"] = root_path
  end

  describe 'switch' do
    it 'select user currency' do
      get :switch,  currency:'uah'
      expect(controller.params[:currency]).not_to be_nil
      expect(controller.params[:currency]).to eq('uah')
      expect(response.cookies['user_currency']).to eq('uah')
    end
  end

end