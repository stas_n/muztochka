require 'spec_helper'

describe SubcategoriesController do
  render_views
  before(:all) do
    @content = create(:content)
    @category = create(:category)
    @subcategory = create(:subcategory, category: @category)
    @detail = create(:detail)
    @brand = create(:brand)
  end


  describe 'show action' do
    it 'renders the category' do
      get :show, category_id: @category.id, id: @subcategory.slug
      expect(response).to render_template('show')
    end


    it 'params[:search]' do
      get :show, category_id: @category.id, id: @subcategory.slug, search: 'product name'
      expect(response).to render_template('show')
      expect(controller.params[:search]).not_to be_nil
      expect(controller.params[:search]).to eq('product name')
    end

    it 'params[:brands]' do
      get :show, category_id: @category.id, id: @subcategory.slug, brands: [@brand.id]
      expect(response).to render_template('show')
      expect(controller.params[:brands]).not_to be_nil
      expect(controller.params[:brands]).to eq([@brand.id.to_s])
    end

    it 'params[:main_filter]' do
      get :show, category_id: @category.id, id: @subcategory.slug, main_filter: 'handmade'
      expect(response).to render_template('show')
      expect(controller.params[:main_filter]).not_to be_nil
      expect(controller.params[:main_filter]).to eq('handmade')
    end

    it 'params[:years]' do
      get :show, category_id: @category.id, id: @subcategory.slug, min_year: '2001',max_year:'2010'
      expect(response).to render_template('show')
      expect(controller.params[:min_year]).not_to be_nil
      expect(controller.params[:min_year]).to eq('2001')
      expect(controller.params[:max_year]).not_to be_nil
      expect(controller.params[:max_year]).to eq('2010')

    end

  end

end

