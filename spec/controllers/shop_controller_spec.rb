require 'spec_helper'

describe ShopsController do
  render_views
  before(:all) do
    @user = create(:user)
    @shop = @user.shop
  end

  before(:each) do
    stub_request(:get, 'http://api.vk.com/method/database.getCountries?count=1000&lang=ru0&need_all=1&v=5.5').
        with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Host' => 'api.vk.com', 'User-Agent' => 'Ruby'}).
        to_return(:status => 200, :body => {'response' => {'count' => 4,
                                                           'items' =>
                                                               [{'id' => 19, 'title' => 'Австралия'},
                                                                {'id' => 20, 'title' => 'Австрия'},
                                                                {'id' => 5, 'title' => 'Азербайджан'},
                                                                {'id' => 21, 'title' => 'Албания'}]}}.to_json, :headers => {})
  end

  describe 'show' do
    it 'renders the shop' do
      get :show, id: @shop.id
      expect(response).to render_template('show')
    end
  end

  describe 'update' do
    it 'change shop attributes' do
      allow(controller).to receive(:current_user).and_return(@user)
      patch :update, id: @shop.id, shop: {title: 'new_title', description: 'new_description', site: 'new_site'}
      expect(@shop.title).to eq('new_title')
      expect(@shop.description).to eq('new_description')
      expect(@shop.site).to eq('new_site')
      expect(response).to render_template('edit')
    end
  end

  describe 'update_shop_cover' do
    it 'should change shop cover' do
      allow(controller).to receive(:current_user).and_return(@user)
      file = Rack::Test::UploadedFile.new(Rails.root.join('test', 'assets', 'images', 'banners', "2.png"), 'image/png')
      patch :update, id: @shop.id, shop: {cover: file}
      expect(@shop.cover_file_name).to eq('2.png')
    end
  end


  describe 'delete_shop_cover' do
    it 'should remove  shop cover' do
      allow(controller).to receive(:current_user).and_return(@user)
      patch :update, id: @shop.id, shop: {cover:nil}
      expect(@shop.cover_file_name).to be_nil
    end
  end
end
