require 'spec_helper'

describe ErrorsController do
  render_views


  describe 'page_not_found' do
    it '404' do
      get 'page_not_found'
      expect(response).to render_template('errors/404')
      expect(response.body).to match /404/
      expect(response.body).to match /Ошибка. Страница не найдена/
    end
  end

  describe 'server_error' do
    it 'renders the list of countries' do
      get 'server_error'
      expect(response).to render_template('errors/500')
      expect(response.body).to match /500/
      expect(response.body).to match /Ошибка. Страница не найдена/
    end
  end

end

