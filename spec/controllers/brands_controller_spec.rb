require 'spec_helper'

describe BrandsController do
  render_views
  before(:all) do
    @content = create(:content)
    @category = create(:category)
    @subcategory = create(:subcategory,category:@category)
    @detail = create(:detail)
    @brand = create(:brand)
  end

  describe 'index' do
    it 'renders the list of brands' do
      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :index
      expect(response).to render_template("index")
    end

    it 'params[:alpha]' do
      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :index, :alpha => 'famous_brand'
      expect(response).to render_template("index")
      expect(controller.params[:alpha]).not_to be_nil
      expect(controller.params[:alpha]).to eq('famous_brand')
    end

    it 'params[:digit]' do
      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :index, :digit => 'digit'
      expect(response).to render_template("index")
      expect(controller.params[:digit]).not_to be_nil
      expect(controller.params[:digit]).to eq('digit')
    end
  end

  describe 'show action' do
    it 'renders the brand' do
      get :show, id: @brand.id
      expect(response).to render_template("show")
    end
  end

  describe 'prepare product' do
    it 'list of filters' do
      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :index, :alpha => 'kiev',page:1,category:@category.title,subcategory:@subcategory.id,
          detail:@detail.title,brands:@brand.title,min_year:2000,max_year:2010
      expect(response).to render_template('index')
      expect(controller.params[:page]).not_to be_nil
      expect(controller.params[:page]).to eq(1.to_s)
      expect(controller.params[:category]).not_to be_nil
      expect(controller.params[:category]).to eq(@category.title)
      expect(controller.params[:subcategory]).not_to be_nil
      expect(controller.params[:subcategory]).to eq(@subcategory.id.to_s)
      expect(controller.params[:detail]).not_to be_nil
      expect(controller.params[:detail]).to eq(@detail.title)
      expect(controller.params[:brands]).not_to be_nil
      expect(controller.params[:brands]).to eq(@brand.title)
      expect(controller.params[:min_year]).not_to be_nil
      expect(controller.params[:min_year]).to eq(2000.to_s)
      expect(controller.params[:max_year]).not_to be_nil
      expect(controller.params[:max_year]).to eq(2010.to_s)
    end
  end
end

