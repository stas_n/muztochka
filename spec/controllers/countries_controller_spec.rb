require 'spec_helper'

describe CountriesController do
  render_views
  before(:all) do
    @content = create(:content)
    @country = create(:country)
    @category = create(:category)
    @subcategory = create(:subcategory,category:@category)
    @detail = create(:detail)
    @brand = create(:brand)
  end

  describe 'index' do
    it 'renders the list of countries' do
      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :index
      expect(response).to render_template('index')
    end
  end


  describe 'show action' do
    it 'renders the country' do
      get :show, id: @country.permalink
      expect(response).to render_template('show')
    end

    it 'params[:category]' do

      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :show, id: @country.permalink, category: @category.id
      expect(response).to render_template('show')
      expect(controller.params[:category]).not_to be_nil
      expect(controller.params[:category]).to eq(@category.id.to_s)
    end
   end

  describe 'prepare product' do
    it 'list of filters' do
      allow(controller).to receive(:set_seo).and_return(@content.seo_title)
      get :index,page:1,category:@category.title,subcategory:@subcategory.id,
          detail:@detail.title,brands:@brand.title,min_year:1960,max_year:2000
      expect(response).to render_template('index')
      expect(controller.params[:page]).not_to be_nil
      expect(controller.params[:page]).to eq(1.to_s)
      expect(controller.params[:category]).not_to be_nil
      expect(controller.params[:category]).to eq(@category.title)
      expect(controller.params[:subcategory]).not_to be_nil
      expect(controller.params[:subcategory]).to eq(@subcategory.id.to_s)
      expect(controller.params[:detail]).not_to be_nil
      expect(controller.params[:detail]).to eq(@detail.title)
      expect(controller.params[:brands]).not_to be_nil
      expect(controller.params[:brands]).to eq(@brand.title)
      expect(controller.params[:min_year]).not_to be_nil
      expect(controller.params[:min_year]).to eq(1960.to_s)
      expect(controller.params[:max_year]).not_to be_nil
      expect(controller.params[:max_year]).to eq(2000.to_s)
    end
  end
end

