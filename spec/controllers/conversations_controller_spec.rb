require 'spec_helper'

describe ConversationsController do
  render_views
  login_user

  before(:all) do
    @sender = create(:user)
    @recipient = create(:user)
    @content = create(:content)
    @product = create(:product,title:'uh')
    @conversation = create(:conversation, sender: @sender, recipient: @recipient)

  end

  describe 'index' do
    it 'display all conversations' do
      allow(controller).to receive(:current_user).and_return(@sender)

      get :index
      expect(response).to render_template('index')
    end

    it 'search' do
      allow(controller).to receive(:current_user).and_return(@sender)

      get :index, sentence: @sender.name
      expect(response).to render_template('index')
      expect(controller.params[:sentence]).not_to be_nil
      expect(controller.params[:sentence]).to eq(@sender.name)
    end
  end

  describe 'create' do
    it 'new chat' do
      allow(controller).to receive(:current_user).and_return(@sender)
      conversation_params = FactoryGirl.attributes_for(:conversation,user_id:3,product_id:4)
      expect { post :create,  conversation_params }.to change(Conversation, :count).by(1)
    end
  end

  describe 'show' do
    it 'renders the conversation' do
      allow(controller).to receive(:current_user).and_return(@sender)
      get :show, id: @conversation.id
      expect(response).to render_template('show')
    end
  end

  describe 'destroy' do
    it 'hide the conversation' do
      allow(controller).to receive(:current_user).and_return(@sender)
      @conversation.update(show_for_sender: false)
      expect(@conversation.show_for_sender).to be false
      expect(@conversation.show_for_recipient).to be true
    end
  end


end

