require 'spec_helper'

describe FavoritesController do
  render_views
  before(:all) do
    @product = create(:product)
    @user = create(:user)
    @favorite = create(:favorite)
  end

  describe 'create' do
    it 'favorite product' do
      allow(controller).to receive(:current_user).and_return(@user)
      xhr :post, :create, {product_id: @product.id, user: @user}
      expect(response).to render_template('favorites/success')
    end
  end

  describe 'destroy' do
    it 'favorite product' do
      @request.env['HTTP_REFERER'] = "{/products/#{@product.id}"
      expect { delete :destroy, :id => @favorite.id }.to change(Favorite, :count).by(-1)
    end
  end

end

