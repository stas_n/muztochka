require 'spec_helper'

describe CategoriesController do
  render_views
  before(:all) do
    @content = create(:content)
    @category = create(:category)
    @subcategory = create(:subcategory,category:@category)
    @detail = create(:detail)
    @brand = create(:brand)
  end


  describe 'show action' do
    it 'renders the category' do
      get :show, id: @category.id
      expect(response).to render_template('show')
    end

    it 'params[:category]' do
      get :show, id: @category.id, category: @category.title
      expect(response).to render_template('show')
      expect(controller.params[:category]).not_to be_nil
      expect(controller.params[:category]).to eq(@category.title)
    end

    it 'params[:subcategory]' do
      get :show, id: @category.id, category: @category.title, subcategory: @subcategory.id
      expect(response).to render_template('show')
      expect(controller.params[:subcategory]).not_to be_nil
      expect(controller.params[:subcategory]).to eq(@subcategory.id.to_s)
    end

    it 'params[:search]' do
      get :show, id: @category.id, category: @category.title,search:'product name'
      expect(response).to render_template('show')
      expect(controller.params[:search]).not_to be_nil
      expect(controller.params[:search]).to eq('product name')
    end

    it 'params[:brands]' do
      get :show, id: @category.id, category: @category.title, brands:[@brand.id]
      expect(response).to render_template('show')
      expect(controller.params[:brands]).not_to be_nil
      expect(controller.params[:brands]).to eq([@brand.id.to_s])
    end

    it 'params[:main_filter]' do
      get :show, id: @category.id, category: @category.title, main_filter:'handmade'
      expect(response).to render_template('show')
      expect(controller.params[:main_filter]).not_to be_nil
      expect(controller.params[:main_filter]).to eq('handmade')
    end

  end

end

