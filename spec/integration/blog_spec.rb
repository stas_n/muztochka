require 'spec_helper'


feature 'Post', integration: true, :js => true do
  scenario 'show blog' do

    article = create(:post)
    lesson = create(:post, category: 'урок')
    overview = create(:post, category: 'обзор')
    photo = create(:post, category: 'фото')

    visit '/blog'
    expect(page).to have_content("Блог")
    expect(page).to have_content("регистрация")
    expect(page).to have_content("Вход")
    expect(page).to have_content("Статьи")
    expect(page).to have_content("Обзоры")
    expect(page).to have_content("Уроки")
    expect(page).to have_content("Фото")
    find('.butt_blog_index').click

    expect(page).to have_content(article.title)
    expect(page).to have_content(article.author)
    expect(page).to have_content(article.article)
    expect(page).to have_content("Читайте также")
    expect(page).to have_content("Поделиться")

    find('.title_top_blog_index', text: 'Блог').click
    expect(page).to have_content("ВЕСЬ ROCK’N’ROLL")

    find('.header_tab_text_blog', text: 'Статьи').click
    URI.parse(current_url).request_uri =='/blog?articles=1'
    expect(page).to have_content(article.title)
    expect(page).to have_content(article.author)
    expect(page).to have_content('Статьи')

    find('.header_tab_text_blog', text: 'Обзоры').click
    URI.parse(current_url).request_uri =='/blog?overviews=1'
    expect(page).to have_content(overview.title)
    expect(page).to have_content(overview.author)
    expect(page).to have_content('Обзоры')

    find('.header_tab_text_blog', text: 'Уроки').click
    URI.parse(current_url).request_uri =='/blog?lessons=1'
    expect(page).to have_content(lesson.title)
    expect(page).to have_content(lesson.author)
    expect(page).to have_content('Уроки')

    find('.header_tab_text_blog', text: 'Фото').click
    URI.parse(current_url).request_uri =='/blog?photos=1'
    expect(page).to have_content(photo.title)
    expect(page).to have_content(photo.author)
    expect(page).to have_content('Фото')
  end


end

