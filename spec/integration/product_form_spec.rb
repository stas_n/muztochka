require 'spec_helper'



feature 'Product', integration: true, :js => true do
  scenario 'form page' do

    4.times { create(:advices) }
    category = create(:category, title: 'Бас-гитары')
    sub_category = create(:subcategory, title: '4-х струнные', category_id: category.id)
    brand = create(:brand,title:'Fender')
    user_login

    find('.button__text', text: 'Продать').click
    find('#category_list').click
    find('li', text: category.title).click
    find('#subcat_name').click
    find('li', text: sub_category.title).click
    find('#brand').set(brand.title)
    find('#product_mark').set('x1')
    find('#year_list').click
    find('li', text: '2015').click
    find('#product_color').set('red')
    find('#product_price').set(100)
    find('#condition_list').click
    find('li', text: 'Новое').click
    find('#country_producing').click
    find('#country_producing').set('Украина')
    find('li', text: 'Украина').click
    find('body').click
    find('.checkbox__input').click
    find('#formatted_address').set('Киев, Украина')
    find('body').click

    sleep 1
    page.attach_file('image_input', Rails.root.to_s+'/test/assets/images/products/product-1.jpg')


    find('#product_video').set('https://www.youtube.com/watch?v=StzfQEruOyw')
    find('#product_description').set('text')
    sleep 1

    click_button('Опубликовать')

  end
end

