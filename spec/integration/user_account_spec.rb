require 'spec_helper'



feature 'User', integration: true, :js => true do
  scenario 'account settings' do

    user_login
    product = create(:product,title:'first_product',shop_id:@user.shop.id)
    product1 = create(:product,title:'second_product',shop_id:@user.shop.id)
    find('.user-avatar__img').click
    find('.user-menu__store').click
    expect(page).to have_content(@user.shop.title)

    expect(page).to have_content("На Музточке с #{I18n.l @user.shop.created_at.to_date, format: :long}")
    find('.user-avatar__img').click
    find('.user-menu__link', text: 'Аккаунт').click
    find('#user_name').set('name')
    find('#user_surname').set('surname')
    find('#user_email').set('new@email.com')
    find('#submit', text: 'Сохранить').click
    expect(page).to have_content('Информация обновлена')


    find('.header-tab__text', text: 'Товары').click
    find('#q').set(product.title)
    find('#submit_q').click
    expect(page).to have_content(product.title)
    expect(page).to have_no_content(product1.title)
    find('.ios-check__label-switch').click
    expect(page).to have_content('Не активен')
    find('.filtering__sort_by').click
    find("li", :text => "Активные").click
    expect(page).to have_content('По вашему запросу ничего не найдено')
    find('#q').set('')
    find('#submit_q').click
    expect(page).to have_content(product1.title)
    find('.ios-check__label-switch').click
    expect(page).to have_content('Не активен')
    expect(page).to have_content(product1.title)
    find('.ios-check__label-switch').click
    expect(page).to have_content('Активен')
    find('.filtering__sort_by').click
    find("li", :text => "Все").click
    expect(page).to have_content(product.title)
    expect(page).to have_content(product1.title)

    find('.header-tab__text', text: 'Сообщения').click


    find('.header-tab__text', text: 'Избранное').click
    expect(page).to have_content('Вы еще не добавили товар')
    expect(page).to have_content('Посмотреть предложения')


  end
end

