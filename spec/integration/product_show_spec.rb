require 'spec_helper'


feature 'Product', integration: true, :js => true do
  scenario 'show page' do

    p = create(:product,price: 100, title: 'first product')
    user_login
    visit '/'

    find('.brick-item__title', text: p.title).click
    expect(page).to have_content(p.title)
    expect(page).to have_content(p.price.to_i)
    expect(page).to have_content('Просмотров 1')
    expect(page).to have_content(p.shop.user.name)
    expect(page).to have_content('Перейти в магазин')
    expect(page).to have_content('Связаться с продавцом')
    expect(page).to have_content('Поделиться')
    expect(page).to have_content('Спецификация')
    expect(page).to have_content('Видео')
    expect(page).to have_content(p.description)
    expect(page).to have_content('Состояние')
    expect(page).to have_content(p.condition)
    expect(page).to have_content('Бренд')
    expect(page).to have_content(p.brand.title)
    expect(page).to have_content('Цвет')
    expect(page).to have_content(p.color)
    expect(page).to have_content('Категория')
    expect(page).to have_content(p.category.title)
    expect(page).to have_content('Год выпуска')
    expect(page).to have_content(p.year)
    expect(page).to have_content('Похожие предложения')

  end
end

