require 'spec_helper'

feature 'Currency switch', integration: true, :js => true do
  scenario 'use currency switcher' do
    create(:product)
    p = create(:product,price:0.02)
    user_login
    visit '/'

    first('.btn-currency').click
    first('.currency_uah').click
    first('#change_currency').click
    expect(page).to have_content('2 506') #TODO capybara cookies access
    expect(page).to have_content('0,50')
    expect(page).to have_content('Макс. цена ₴')

    first('.btn-currency').click
    first('.currency_rub').click
    first('#change_currency').click
    expect(page).to have_content('6 40')
    expect(page).to have_content('1')
    expect(page).to have_content('Макс. цена ₽')


    first('.btn-currency').click
    first('.currency_usd').click
    first('#change_currency').click
    expect(page).to have_content('100')
    expect(page).to have_content('0,02')
    expect(page).to have_content('Макс. цена $')


    first('.btn-currency').click
    first('.currency_eur').click
    first('#change_currency').click
    expect(page).to have_content('89')
    expect(page).to have_content('0,02')
    expect(page).to have_content('Макс. цена €')


    first('.btn-currency').click
    first('#change_currency').click
    expect(page).to have_content('90')
  end
end