require 'spec_helper'

feature 'Search', integration: true, :js => true do
  scenario 'use checkboxes and inputs' do

    brand = create(:brand, title: 'Fender')
    brand1 = create(:brand, title: 'APM')
    p = create(:product, brand_id: brand.id, price: 100, title: 'first product',year:2000)
    p1 = create(:product, brand_id: brand1.id, price: 200, title: 'second product',year:2010)

   user_login

    find(".checkbox__label", :text =>brand.title ).click #brand
    expect(page).to have_content(p.title)
    find(".checkbox__label", :text =>brand.title ).click
    expect(page).to have_content(p.title)
    expect(page).to have_content(p1.title)
    find(".checkbox__label", :text =>brand1.title ).click
    expect(page).to have_content(p1.title)
    find(".checkbox__label", :text =>brand1.title ).click

    find('#max_price').set(100) #sort
    click_link('Фильтр')
    expect(page).to have_content(p.title)
    find('#max_price').set(10)
    click_link('Фильтр')
    expect(page).to have_no_content(p.title)
    expect(page).to have_no_content(p1.title)
    find('#max_price').set(200)
    find('.filtering__sort').click
    find("li", :text => "По возрастанию цены").click
    click_link('Фильтр')
    expect(p.title).to appear_before(p1.title)
    find('.filtering__sort').click
    find("li", :text => "По убыванию цены").click
    click_link('Фильтр')
    expect(p1.title).to appear_before(p.title)

    find('.filter-list__title',text:'с 2011').click #year
    expect(page).to have_no_content(p.title)
    expect(page).to have_no_content(p1.title)
    find('.filter-list__title',text:'2001 - 2010').click
    expect(page).to have_content(p1.title)
    expect(page).to have_content(p1.price.to_i)
    find('.filter-list__title',text:'Все года').click
    expect(page).to have_content(p.title)
    expect(page).to have_content(p1.title)

    find('.header_search').click #header search
    find('.header_search').set(p.title)
    find('#submit_search').click
    expect(page).to have_content(p.title)
    expect(page).to have_content(p.price.to_i)
    find('.header_search').click
    find('.header_search').set('random text')
    find('#submit_search').click
    expect(page).to have_no_content(p.title)
    expect(page).to have_no_content(p1.title)

    find('.filter-list__title',text: p.category.title).click #category
    expect(page).to have_content(p.category.title)
    expect(page).to have_content(p.category.text)
    expect(page).to have_content(p.title)
    expect(page).to have_content(p.price.to_i)
    expect(page).to have_no_content(p1.title)
  end

end

