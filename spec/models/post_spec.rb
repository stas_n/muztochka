require 'spec_helper'

describe Post do

  it 'has a valid factory' do
    post = create(:post)
    expect(post).to be_valid
  end


  it 'categories' do
    expect(Post.categories).to match_array( %w(статья урок обзор фото))
  end


  it 'raises an error if photo blank' do
   #  # post=create(:post)
   #  post.errors.full_messages.should include('Добавьте фото')
  end

  it 'check nested attributes' do
    should accept_nested_attributes_for(:photo).allow_destroy(true)
    should accept_nested_attributes_for(:post_photos).allow_destroy(true)
  end
end
