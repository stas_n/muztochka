require 'spec_helper'

describe PromoItem do

  it 'assosiations' do
    expect(PromoItem.reflect_on_association(:product).macro).to eq(:belongs_to)
    expect(PromoItem.reflect_on_association(:promo_service).macro).to eq(:belongs_to)
  end

  it 'statuses' do
    expect(PromoItem::STATUSES).to match_array(["pending", "success", "finished"])
  end

  it 'decrease_count' do
    promo_item = create(:promo_item)
    expect{promo_item.decrease_count}.to change{promo_item.view_count}.from(1).to(0)
  end

  it 'payment_statuses' do
    expect(PromoItem.payment_statuses).to match_array(["processing", "success", "failure", "wait_accept", "wait_secure"])
  end
end

