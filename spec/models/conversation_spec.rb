require 'spec_helper'

describe Conversation do

  before(:each) do
    # @conversation = create(:conversation)
  end

  it 'has a valid factory' do
    # expect(@city).to be_valid
  end

  it 'assosiations' do
    should belong_to(:sender).class_name('User')
    should belong_to(:recipient).class_name('User')
    should belong_to(:product)
    expect(Conversation.reflect_on_association(:messages).macro).to eq(:has_many)
  end


end