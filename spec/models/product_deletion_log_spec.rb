require 'spec_helper'

describe ProductDeletionLog do

  it 'has a valid factory' do
    product_deletion_log = create(:product_deletion_log)
    expect(product_deletion_log).to be_valid
  end

  it 'assosiations' do
    expect(ProductDeletionLog.reflect_on_association(:user).macro).to eq(:belongs_to)
    expect(ProductDeletionLog.reflect_on_association(:reason).macro).to eq(:belongs_to)
    expect(ProductDeletionLog.reflect_on_association(:product).macro).to eq(:belongs_to)
  end

end

