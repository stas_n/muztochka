require 'spec_helper'

describe Photo do

  it 'assosiations' do
    expect(Photo.reflect_on_association(:product).macro).to eq(:belongs_to)
  end


  it 'validate photo size' do
   should validate_attachment_size(:image).in(0..5.megabytes )
   should validate_attachment_content_type(:image).allowing('image/png', 'image/gif','image/jpeg','image/jpg')
  end
end