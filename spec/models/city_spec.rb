require 'spec_helper'

describe City do

  before(:each) do
    @city = create(:city)
  end

  it 'has a valid factory' do
    expect(@city).to be_valid
  end

  it 'assosiations' do
    expect(City.reflect_on_association(:country).macro).to eq(:belongs_to)
  end

  it 'set_seo' do
    city_no_name = create(:city,name:'')
    expect(city_no_name.seo_title).to eq("Музыкальные товары в г. " + city_no_name.name + " - купить недорого на Муzточка")
    expect(@city.seo_title).to eq(@city.name)
  end

  it 'set_permalink' do
    expect(@city.set_permalink).to eq(@city.name)
  end

end