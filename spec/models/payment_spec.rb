require 'spec_helper'

describe PaymentType do

  it 'assosiations' do
    expect(PaymentType.reflect_on_association(:shops).macro).to eq(:has_and_belongs_to_many)
  end

end