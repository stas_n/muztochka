require 'spec_helper'

describe Brand do

  before(:each) do
    @brand = create(:brand)
  end

  it 'has a valid factory' do
    expect(@brand).to be_valid
  end

  it 'has_many assosiations' do
    expect(Brand.reflect_on_association(:products).macro).to eq(:has_many)
  end

  it 'set_seo' do
    expect(@brand.seo_title).to eq(@brand.title + ' купить по цене от 50$ на muztochka.com')
    expect(@brand.seo_description).to eq('Хочешь купить ' + @brand.title + '?' + ' ' + 'Покупай на Музточке: б/у и новые ' + @brand.title + '. Muztochka.com - товары напрямую от продавцов. Выгодные цены. Удобный сервис.')
  end
  
end