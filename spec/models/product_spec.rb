require 'spec_helper'

describe Product do


  before(:each) do
    @product = create(:product)
  end

  it 'has a valid factory' do
    expect(@product).to be_valid
  end

  it 'assosiations' do
    expect(Product.reflect_on_association(:category).macro).to eq(:belongs_to)
    expect(Product.reflect_on_association(:subcategory).macro).to eq(:belongs_to)
    expect(Product.reflect_on_association(:detail).macro).to eq(:belongs_to)
    expect(Product.reflect_on_association(:brand).macro).to eq(:belongs_to)
    expect(Product.reflect_on_association(:shop).macro).to eq(:belongs_to)
    expect(Product.reflect_on_association(:promo_items).macro).to eq(:has_many)
    expect(Product.reflect_on_association(:promo_items).macro).to eq(:has_many)
    expect(Product.reflect_on_association(:visitors_count).macro).to eq(:has_many)
    expect(Product.reflect_on_association(:deletion_logs).macro).to eq(:has_many)
    expect(Product.reflect_on_association(:photos).macro).to eq(:has_many)
  end

  it 'check enum' do
    should define_enum_for(:permission).with(%w( active non_active deleted ))
  end

  it 'validate_photo_error' do
    # @product.validate_photos('[]')
    # expect(@product.errors.size).to eq(1)
  end

  it 'get conditions' do
    expect(Product.get_conditions).to match_array(['Новое', 'Как новое', 'Отличное', 'Очень хорошее',
                                                   'Хорошее', 'Удовлетворительное', 'Плохое', 'Нерабочее'])
  end

  it 'subcategory' do
    expect(Product.get_subcategories).to match_array(Subcategory.all.map { |i| [ "#{i.title} (#{i.category.title})", "#{i.id}"] }.sort)
  end

  it 'get details' do
    expect(Product.get_details).to match_array(Detail.all.map { |i| [ "#{i.title} (#{i.category.title})", "#{i.id}"] }.sort)
  end

  it 'search' do
    expect(Product.search(@product.title)).to include(@product)
    expect(Product.search(@product.mark)).to include(@product)
    expect(Product.search('123')).not_to include(@product)
  end

  it 'q' do
    expect(Product.search(@product.title)).to include(@product)
    expect(Product.search(@product.mark)).to include(@product)
  end

  it 'alpha' do
    expect(Product.alpha(@product.city)).to include(@product)
    expect(Product.alpha('123')).not_to include(@product)
  end

  it 'count_of_visits'do
    expect(@product.count_of_visits).eql?(0)
  end

  it 'active?' do
    expect(@product.permission).eql?('active')
  end

  it 'deleted?' do
    expect(@product.permission).eql?('deleted')
  end

  it 'remake_slug' do
    expect(@product.remake_slug).to be true
  end

  it 'slug_candidates' do
    expect(@product.slug_candidates).eql?([:title, [:title, :id]])
  end

  it 'titles' do
    expect(Product.titles).eql?(Product.all.map(&:title))
  end

  it 'in_list?' do
    expect(Product.in_list?('>',@product.year)).to be true
    expect(Product.in_list?('<',@product.year)).to be true
    expect(Product.in_list?(1990,@product.year)).to be true
  end
end