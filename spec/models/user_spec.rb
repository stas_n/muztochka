require 'spec_helper'

describe User do

  it 'check_name' do
    user =  create(:user,name:'')
    expect(user.name).to eq(user.email.split("@").first)
  end

  it 'assosiations' do
    should have_many(:social_accounts).dependent(:destroy)
    should have_many(:favorites).dependent(:destroy)
    should have_one(:shop).dependent(:destroy)
    should have_many(:conversations).class_name('Conversation').with_foreign_key('sender_id')
    should have_many(:messages)
  end

  it 'validate avatar ' do
    should validate_attachment_size(:avatar).in(0..5.megabytes )
    should validate_attachment_content_type(:avatar).allowing('image/png', 'image/gif','image/jpeg','image/jpg')
  end

  it 'count_all_unread_messages' do
    user =  create(:user)
    expect(user.count_all_unread_messages).to eq(0)
  end
end

