require 'spec_helper'

describe Category do

  it 'has a valid factory' do
    @category = create(:category)
    expect(@category).to be_valid
  end

  it 'has_many assosiations' do
    expect(Category.reflect_on_association(:subcategories).macro).to eq(:has_many)
    expect(Category.reflect_on_association(:details).macro).to eq(:has_many)
    expect(Category.reflect_on_association(:products).macro).to eq(:has_many)
  end

  it 'check nested attributes' do
    should accept_nested_attributes_for(:details).allow_destroy(true)
    should accept_nested_attributes_for(:subcategories).allow_destroy(true)
  end

  it 'set seo' do
    @category = create(:category)
    expect(@category.seo_title).to eq(@category.title + ' купить по цене от 50$ на muztochka.com')
    expect(@category.seo_description).to eq('Хочешь купить ' + @category.title + '?' + ' ' + 'Покупай на Музточке: б/у и новые ' + @category.title + '. Muztochka.com - товары напрямую от продавцов. Выгодные цены. Удобный сервис.')
  end


end
