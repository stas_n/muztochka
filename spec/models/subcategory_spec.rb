require 'spec_helper'

describe Subcategory do


  it 'has a valid factory' do
    subcategory = create(:subcategory)
    expect(subcategory).to be_valid
  end

  it 'assosiations' do
    expect(Subcategory.reflect_on_association(:category).macro).to eq(:belongs_to)
    expect(Subcategory.reflect_on_association(:products).macro).to eq(:has_many)
  end

  it 'validate photo size' do
    should validate_attachment_size(:image).in(0..5.megabytes )
    should validate_attachment_content_type(:image).allowing('image/png', 'image/gif','image/jpeg','image/jpg')
  end

end