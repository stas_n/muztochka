require 'spec_helper'

describe Country do

  before(:each) do
    @country = create(:country)
  end

  it 'has a valid factory' do
    expect(@country).to be_valid
  end

  it 'has_many assosiations' do
    expect(Country.reflect_on_association(:cities).macro).to eq(:has_many)
  end

  it 'set_seo' do
    expect(@country.seo_title).to eq(@country.name)
  end

end