require 'spec_helper'

describe Detail do

  before(:each) do
    @detail = create(:country)
  end

  it 'has a valid factory' do
    expect(@detail).to be_valid
  end

  it 'has_many assosiations' do
    expect(Detail.reflect_on_association(:products).macro).to eq(:has_many)
    expect(Detail.reflect_on_association(:category).macro).to eq(:belongs_to)
  end

end