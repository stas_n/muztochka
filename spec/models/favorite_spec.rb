require 'spec_helper'

describe Favorite do

  before(:each) do
    @favorite = create(:favorite)
  end

  it 'has a valid factory' do
    expect(@favorite).to be_valid
  end

  it 'assosiations' do
    expect(Favorite.reflect_on_association(:user).macro).to eq(:belongs_to)
    expect(Favorite.reflect_on_association(:product).macro).to eq(:belongs_to)
  end


end