require 'simplecov'
SimpleCov.start
require 'rubygems'
require 'spork'
require 'omniauth'
require 'webmock/rspec'

Spork.prefork do
  ENV['RAILS_ENV'] ||= 'test'


  require File.expand_path('../../config/environment', __FILE__)

  require 'capybara/rspec'
  require 'capybara-screenshot/rspec'
  require 'rspec/rails'
  require 'database_cleaner'
  require 'factory_girl'
  require 'devise'
  require 'paperclip/matchers'


  ActiveRecord::Migration.check_pending!
  Dir[Rails.root + 'spec/support/**/*.rb'].map &method(:require)
  RSpec.configure do |config|
    config.include Capybara::DSL
    config.include FactoryGirl::Syntax::Methods

    config.fixture_path = "#{::Rails.root}/spec/fixtures"
    config.mock_with :rspec
    config.order = :random
    config.use_transactional_fixtures = false
    config.use_instantiated_fixtures = true
    config.global_fixtures = :all
    config.infer_spec_type_from_file_location!
    config.raise_errors_for_deprecations!
    config.include Paperclip::Shoulda::Matchers
    config.include Devise::TestHelpers, type: :controller
    config.include Warden::Test::Helpers
    config.include Rails.application.routes.url_helpers
    config.infer_base_class_for_anonymous_controllers = false
    config.include ApplicationHelper, :type => :feature
    config.include ShowMeTheCookies, :type => :feature


    Capybara.default_max_wait_time = 10


    config.before do
      ActionMailer::Base.deliveries.clear
    end



    config.before(:suite) do
      DatabaseCleaner.clean_with(:truncation)
    end

    config.before(:each) do
      DatabaseCleaner.strategy = :transaction
    end

    config.before(:each, :js => true) do
      DatabaseCleaner.strategy = :truncation
    end

    config.before(:each) do
      DatabaseCleaner.start
    end

    config.after(:each) do
      DatabaseCleaner.clean
    end
    config.extend ControllerMacros, :type => :controller

    config.before(:each) do
      allow_any_instance_of(ApplicationController).to receive(:set_seo).and_return('seo_text')
    end

    config.before(:each) do
      rates = {"usd" =>
                   {"date" => "2016-08-17 00:00:00",
                    "ask" => "25.05928800",
                    "bid" => "25.05928800",
                    "trendAsk" => "-0.05585300",
                    "trendBid" => "-0.05585300",
                    "currency" => "usd"},
               "eur" =>
                   {"date" => "2016-08-17 00:00:00",
                    "ask" => "28.30446600",
                    "bid" => "28.30446600",
                    "trendAsk" => "0.22573800",
                    "trendBid" => "0.22573800",
                    "currency" => "eur"},
               "rub" =>
                   {"date" => "2016-08-17 00:00:00",
                    "ask" => "0.39185000",
                    "bid" => "0.39185000",
                    "trendAsk" => "0.00069000",
                    "trendBid" => "0.00069000",
                    "currency" => "rub"}}.to_json

      stub_request(:get, 'http://api.minfin.com.ua/nbu/aca0f2bcfc5a454366e82c0baa8657d7d8a45a06').
          to_return(body:rates, status: 200)
    end

  end

  OmniAuth.config.test_mode = true
  Warden.test_mode!

  Shoulda::Matchers.configure do |config|
    config.integrate do |with|
      with.test_framework :rspec
      with.library :rails
    end
  end

  Capybara::Screenshot.prune_strategy = { keep: 10 }
  WebMock.disable_net_connect!(allow_localhost: true)
  Capybara.ignore_hidden_elements = false
  Capybara::Screenshot.webkit_options = { width: 1980, height: 1200 }

  Capybara.register_driver :selenium_mobile do |app|
    args = []
    args << "-user-agent='Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1'"
    Capybara::Selenium::Driver.new(app, :browser => :chrome, :args => args)
  end

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
end


