def user_login
  @user = create(:user)
  create(:content)
  [1, 2, 3, 4].each { |x| create(:banner, position: x) }
  visit '/'

  find('#login_button_two').click
  within('.form_login') do
    find('#user_email').set(@user.email)
    find('#user_password').set(@user.password)
    find('.button_type_default').click
  end
end

def mobile_user_login
  @user = create(:user)
  create(:content)
  [1, 2, 3, 4].each { |x| create(:banner, position: x) }
  visit '/'

  find('#open_top_menu').click
  find('#m_login').click
  within('#reg_registration_show') do
    find('#reg_mail_2').set(@user.email)
    find('#pass').set(@user.password)
    find('.button_reg_mail_2').click
  end
end