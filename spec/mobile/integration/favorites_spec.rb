require 'spec_helper'


feature 'User', integration: true, driver: :selenium_mobile, :js => true do

  scenario 'favorite products' do
    4.times { create(:advices) }

    20.times { create(:product) }

    mobile_user_login
    sleep 1
    first('.item_first').click
    product = Product.first
    expect(page).to have_content(product.title)
    expect(page).to have_content("Цена $ #{product.price.to_i}")
    expect(page).to have_content('Следят за товаром  0')
    expect(page).to have_content('Информация о продавце')
    expect(page).to have_content('Спецификация')
    expect(page).to have_content('Видео')
    expect(page).to have_content(product.description)
    expect(page).to have_content('Состояние')
    expect(page).to have_content(product.condition)
    expect(page).to have_content('Бренд')
    expect(page).to have_content(product.brand.title)
    expect(page).to have_content('Цвет')
    expect(page).to have_content(product.color)
    expect(page).to have_content('Категория')
    expect(page).to have_content(product.category.title)
    expect(page).to have_content('Год выпуска')
    expect(page).to have_content(product.year)
    expect(page).to have_content('Похожие предложения')
    find('.button-favorite', text: 'ДОБАВИТЬ В ИЗБРАННОЕ').click

    expect(page).to have_content('УДАЛИТЬ ИЗ ИЗБРАННОГО')
    expect(page).to have_content('Следят за товаром  1')
    Product.all.each { |p| @user.favorites.create(product_id: p.id) } #simulate favorites for pagination
    sleep 1
    find('#open_top_menu').click
    find('.favorites_item').click
    expect(page).to have_content('Избранное')
    expect(page).to have_content('Следующая')
    find('.next_page').click
    expect(page).to have_content('Предыдущая')
    expect(page).to have_content('Следующая')


  end
end