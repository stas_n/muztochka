FactoryGirl.define do
  factory :product do
    sequence(:title) { |n| Faker::Name.title+"#{n}" }
    description Faker::Lorem.sentence
    mark Faker::Book.genre
    year Date.today.year
    color Faker::Color.color_name
    condition 'new'
    handmade true
    price 100
    video 'https://www.youtube.com/watch?v=9tCP-s0auOk'
    country Faker::Address.country
    state Faker::Address.state
    city Faker::Address.city
    category
    subcategory
    brand
    shop
    after(:create) { |product| product.photos << FactoryGirl.create(:photo) }

  end
end

