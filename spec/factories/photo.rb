FactoryGirl.define do
  factory :photo, aliases: [:photos] do
    # product
    position 1
    image File.open(Rails.root.join('test', 'assets', 'images', 'banners', (1..6).to_a.sample.to_s + '.png'))

  end
end
