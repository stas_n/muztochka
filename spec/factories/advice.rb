FactoryGirl.define do
    factory :advices do
      sequence(:title) { |n| Faker::Company.name+"#{n}" }
      description Faker::Lorem.sentence
    end
end