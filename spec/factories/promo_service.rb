FactoryGirl.define do
  factory :promo_service do
    title Faker::Name.title
    view_count 1
    price 100
  end
end
