FactoryGirl.define do
  factory :category do
    sequence(:title) { |n| Faker::Hipster.word+"#{n}" }
    position 1
    text Faker::Lorem.sentence
    image File.open(Rails.root.join('test', 'assets', 'images', 'banners', (1..6).to_a.sample.to_s+'.png'))

  end
end




