FactoryGirl.define do
  factory :country do
    name Faker::Address.country
    permalink true
    seo_text Faker::Lorem.sentence
    image_file_name Faker::Avatar.image
  end
end
