FactoryGirl.define do
  factory :product_deletion_reason,aliases: [:reason] do
    name_ru Faker::Name.title
    is_active true
  end
end
