FactoryGirl.define do
  factory :brand do
    sequence(:title) { |n| Faker::Company.name+"#{n}" }
    approved true
    text Faker::Lorem.sentence
    image_file_name Faker::Avatar.image
  end
end
