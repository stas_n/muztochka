FactoryGirl.define do
  factory :post do
    article Faker::Lorem.paragraph(4)
    video 'http://youtube.com'
    author Faker::Name.name
    category 'статья'
    title Faker::Name.title
    photo
    first_paragraph_text Faker::Lorem.paragraph(1)
    after(:create) { |post| post.post_photos << FactoryGirl.create(:posts_photo) }

  end
end