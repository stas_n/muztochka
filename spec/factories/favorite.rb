FactoryGirl.define do
  factory :favorite do
    user
    product
    position 1
  end
end
