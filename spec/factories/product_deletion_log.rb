FactoryGirl.define do
  factory :product_deletion_log do
    product
    user
    reason
  end
end
