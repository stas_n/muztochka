FactoryGirl.define do
  factory :subcategory do
    sequence(:title) { |n| Faker::Company.name+"#{n}" }
    position 1
    category
    text Faker::Lorem.sentence
    image_file_name Faker::Avatar.image
    seo_title Faker::Lorem.sentence
  end
end
