FactoryGirl.define do
  factory :conversation do
    sender_read_at Time.now
    recipient_read_at Time.now + 10.seconds
    sender
    recipient
    product
  end
end
