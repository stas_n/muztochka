FactoryGirl.define do
  factory :content do
    title Faker::Name.title
    description Faker::Lorem.sentence(3)
    seo_title Faker::Name.title
    seo_description Faker::Lorem.sentence(3)

  end
end

