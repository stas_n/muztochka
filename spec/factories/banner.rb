FactoryGirl.define do
  factory :banner do
    sequence(:title) { |n| Faker::Company.name+"#{n}" }
    image File.open(Rails.root.join('test', 'assets', 'images', 'banners', "1.png"))
    link "http://muzdot.coalla.com.ua/"
  end
end