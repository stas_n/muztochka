FactoryGirl.define do
  factory :city do
    name Faker::Address.city
    state Faker::Address.state
    text Faker::Lorem.sentence
    image_file_name Faker::Avatar.image
  end
end
