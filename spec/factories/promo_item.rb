FactoryGirl.define do
  factory :promo_item do
    price 100
    promo_service
    product
    view_count 1
    status 'success'
  end
end
