FactoryGirl.define do
  factory :shop do
    sequence(:title) { |n| Faker::App.name+"#{n}" }
    description Faker::Lorem.sentence
    cover File.open(Rails.root.join('test', 'assets', 'images', 'banners', "1.png"))
    site 'example.com'
    user
  end
end
