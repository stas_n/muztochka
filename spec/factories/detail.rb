FactoryGirl.define do
  factory :detail do
    title Faker::Commerce.product_name
    position 1
    category
  end
end
