FactoryGirl.define do
  factory :user, aliases: [:sender, :recipient] do
    sequence(:email) { |n| Faker::Internet.email + "#{n}" }
    name Faker::Name.name
    surname Faker::Name.last_name
    password "password"
    password_confirmation "password"
    confirmed_at Date.today
    avatar File.open(Rails.root.join('test', 'assets', 'images','avatar.png'))
    after(:create) { |u|u.create_shop(attributes_for(:shop)) }
  end
end
