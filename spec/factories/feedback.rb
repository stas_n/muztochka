FactoryGirl.define do
  factory :feedback do
    name Faker::Company.name
    email Faker::Internet.email
    phone Faker::PhoneNumber.cell_phone
    text Faker::Lorem.sentence

  end
end
