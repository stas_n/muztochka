class BigbandWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: 'load_xml'

  def perform(products=nil)
    @xml = Bigband.new
    @shop_id = Shop.find_by(title: 'Big Band').id

    p 'product deleted'

    p 'Start loading xml...'
    if products.present?
      xml_loop(products)
    else
      begin
        xml_loop(@xml.products)
      rescue OpenURI::HTTPError => error
        puts error.message
      rescue URI::Error => error
        puts error.message
      else
        p 'dead :('
      end
    end
  end


  def check_if_saved(index, p)
    if p.new_record?
      XmlTracker.create(xml_id: index, is_saved: false)
    else
      XmlTracker.create(xml_id: index, is_saved: true)
    end
  end

  def xml_loop(array)
    array.each_with_index do |product, index|
      begin
        p = Product.new(
            title: product['name'],
            description: @xml.set_description(product),
            mark: @xml.set_mark(product),
            year: '',
            price: to_app_price(product['price'], 'uah'),
            condition: 'Новое',
            category: @xml.set_category(product['category']),
            subcategory: @xml.set_subcat(product['subcategory']),
            brand_id: '',
            detail_id: '',
            shop_id: @shop_id,
            country_producing: product['country_producing'],
            country: 'Украина',
            city: 'Киев',
            state: 'город Киев',
            created_at: @xml.rand_time(Time.now, 12.month.ago)

        )


        p.photos << Photo.new(image: URI.parse(product['images']['image'])) if product['images']
        p.save
        check_if_saved(index, p)
        p index


      rescue OpenURI::HTTPError => error
        puts error.message
        next
      end
    end
  end
end