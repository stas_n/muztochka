class FrontmanWorker
  include Sidekiq::Worker

  sidekiq_options retry: false, queue: 'load_xml'

  def perform(products=nil)
    @xml = Frontman.new
    @shop_id = Shop.find_by(title: 'frontman').id
    p 'Start loading xml...'
    if products.present?
      xml_loop(products)
    else
      begin
        xml_loop(@xml.products)
      rescue OpenURI::HTTPError => error
        puts error.message
        index = @xml.get_index(XmlTracker.last.xml_id)
        xml_loop(@xml.products[index...3300])
      rescue URI::Error
        index = @xml.get_index(XmlTracker.last.xml_id)
        xml_loop(@xml.products[index...3300])
      else
        index = @xml.get_index(XmlTracker.last.xml_id)
        xml_loop(@xml.products[index...3300])
      end
    end
  end


  def check_if_saved(product,p)
    if p.new_record?
      XmlTracker.create(xml_id: product['id'], is_saved: false)
    else
      XmlTracker.create(xml_id: product['id'], is_saved: true)
    end
  end

  def xml_loop(array)
    array.each_with_index do |product, index|
      p = Product.new(
          title: product['name'],
          description: product['description'],
          mark: product['vendor'],
          year: '',
          price: to_app_price(product['price'],product['currencyId'].downcase),
          condition: 'Новое',
          category: @xml.set_category(product['categoryId']),
          subcategory: @xml.set_subcat(product['categoryId']),
          brand_id: @xml.set_brand(product['vendor']),
          detail_id: @xml.set_detail(product['categoryId']),
          shop_id: @shop_id,
          country_producing: product['country_of_origin'],
          country: 'Украина',
          city: 'Харьков',
          state: 'город Харьков',
          created_at: @xml.rand_time(Time.now, 12.month.ago)

      )

      if product['picture'].is_a? String
        p.photos << Photo.new(image: URI.parse(product['picture']))
        p.save
        check_if_saved(product,p)
      elsif product['picture'].is_a? Array
        product['picture'].each do |photo|
          p.photos << Photo.new(image: URI.parse(photo))if product['picture']
          p.save
          check_if_saved(product,p)
        end
      end
      p index
    end
  end

end