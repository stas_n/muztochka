class GuitarmaniaWorker
  include Sidekiq::Worker

  sidekiq_options retry: false, queue: 'load_xml'

    def perform(products=nil)
      p 'Start loading xml...'

      @xml = Guitarmania.new
      @shop_id = Shop.find_by(title: "GuitaraMania.ru").id
      if products.present?
        xml_loop(products)
      else
        begin
          xml_loop(@xml.products)
        rescue OpenURI::HTTPError
          index = @xml.get_index(XmlTracker.last.xml_id)
          xml_loop(@xml.products[index...900])
        rescue URI::Error
          index = @xml.get_index(XmlTracker.last.xml_id)
          xml_loop(@xml.products[index...900])
        else
          index = @xml.get_index(XmlTracker.last.xml_id)
          xml_loop(@xml.products[index...900])
        end
      end
    end

  def check_if_saved(product, p)
    if p.new_record?
      XmlTracker.create(xml_id: product['id'], is_saved: false)
    else
      XmlTracker.create(xml_id: product['id'], is_saved: true)
    end
  end

  def xml_loop(array)
    array.each_with_index do |product, index|
      p = Product.new(
          title: product['name'],
          description: product['description'],
          mark: product['param']['__content__'],
          year: '',
          price: to_app_price(product['price'], product['currencyId']),
          condition: 'Новое',
          category: @xml.set_category,
          brand_id: @xml.set_brand(product['param']['__content__']),
          shop_id: @shop_id,
          country: 'Россия',
          city: 'Омск',
          state: 'город Омск',
          created_at: @xml.rand_time(Time.now, 12.month.ago)

      )

      p.photos << Photo.new(image: URI.parse(product['picture'])) if product['picture']
      p.save
      check_if_saved(product, p)

      p index
    end
  end

end