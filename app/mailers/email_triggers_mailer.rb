class EmailTriggersMailer < ActionMailer::Base

  def welcome_email_advantages email
    mail(to: email,
         from: "\"Муzточка\" <hello@muztochka.com>",
         subject: 'Добро пожаловать') do |format|
      format.html { render 'mailer/welcome_advantages' }
    end
  end

  def welcome_email_products email, user_id
    @user_id = user_id
    mail(to: email,
         from: "\"Муzточка\" <hello@muztochka.com>",
         subject: 'Добавьте товары') do |format|
      format.html { render 'mailer/welcome_products' }
    end
  end

  def welcome_email_configure email, slug, user_id
    @slug = slug
    @user_id = user_id
    mail(to: email,
         from: "\"Муzточка\" <hello@muztochka.com>",
         subject: 'Как продавать?') do |format|
           format.html { render 'mailer/welcome_configure' }
    end
  end

  def message_sent_email email, shop_name, shop_slug
    @shop_name = shop_name
    @slug = shop_slug
    mail(to: email,
         from: "\"Муzточка\" <message@muztochka.com>",
         subject: 'Ваше сообщение доставлено') do |format|
      format.html { render 'mailer/message_sent' }
    end
  end

  def message_received_email email, shop_name, shop_slug, conversation_id,recipient_id
    @shop_name = shop_name
    @slug = shop_slug
    @conversation_id = conversation_id
    @recipient_id = recipient_id
    mail(to: email,
         from: "\"Муzточка\" <message@muztochka.com>",
         subject: 'Вы получили новое сообщение') do |format|
      format.html { render 'mailer/message_received' }
    end
  end

  def favorites_add_email email, product_title, product_slug, shop_id
    @product_title = product_title
    @product_slug = product_slug
    @shop_id = shop_id
    mail(to: email,
         from: "\"Муzточка\" <hello@muztochka.com>",
         subject: 'Вам понравился товар ?') do |format|
      format.html { render 'mailer/favorites_add' }
    end
  end
end
