class SubcategoriesController < ApplicationController

  def show
    @subcategory = Subcategory.friendly.find(params[:id])
    @category = @subcategory.category

    if params[:page] == '1'
      redirect_to subcategory_path(@category, @subcategory), :status => 301
    end

    @categories = Category.all
    @brands = Brand.where(id: @subcategory.products.pluck(:brand_id)).take(12)

    @products = @subcategory.products
                            .by_search(params[:search])
                            .by_category(params[:category])
                            .by_subcategory(params[:subcategory])
                            .by_detail(params[:detail])
                            .by_brands(params[:brands])
                            .by_min_year(params[:min_year])
                            .by_max_year(params[:max_year])
                            .by_main_filter(params[:main_filter])
                            .by_sort(params[:sort_by])
                            .by_max_price(params[:max_price])
                            .page(params[:page])
                            .per(30)

    @promo = @subcategory.products
                         .joins(:promo_items)
                         .where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0)
                         .by_brands(params[:brands])
                         .sample(3)

    set_seo(@subcategory)
  end
end
