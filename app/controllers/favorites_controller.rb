class FavoritesController < ApplicationController

  def create
    @product = Product.find(params[:product_id])
    @favorite = Favorite.new(:product => @product, :user => current_user)
    respond_to do |format|
      if @favorite.save
        format.js { render 'success', layout: false }
      else
        format.js { render 'error', layout: false }
      end
    end
  end

  def destroy
    @favorite = Favorite.find(params[:id])
    @product = @favorite.product
    respond_to do |format|
      if @favorite.destroy
        format.html{ redirect_to :back }
        format.js { render 'success', layout: false }
      else
        format.html{ redirect_to :back }
        format.js { render 'error', layout: false }
      end
    end
  end

end