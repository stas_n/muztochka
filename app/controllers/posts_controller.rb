class PostsController < ApplicationController
  def index
    @important_post = Post.important_post || Post.last
    if params[:articles].present?
      @posts = Post.where(category: 'статья').page(params[:page]).per(12)
    elsif params[:lessons].present?
      @posts = Post.where(category: 'урок').page(params[:page]).per(12)
    elsif params[:overviews].present?
      @posts = Post.where(category: 'обзор').page(params[:page]).per(12)
    elsif params[:photos].present?
      @posts = Post.where(category: 'фото').page(params[:page]).per(12)
    else
      @posts = Post.all.page(params[:page]).per(12)
    end
  end

  # def articles
  #   @posts = Post.where(post_category: params[:id]) if @posts
  # end TODO nadoli?

  def show
    @posts = Post.last(3)
    @post = Post.unscoped.friendly.find(params[:id])
  end
end