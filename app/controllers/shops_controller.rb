class ShopsController < ApplicationController
  before_action :set_countries, :only => [:edit]
  before_action :check_user, :only => [:edit, :update]

  def show
    @shop = Shop.friendly.find(params[:id])

    if params[:page] == '1'
      redirect_to @shop, :status => 301
    end

    @categories = Category.where(:id => @shop.products.map(&:category_id))
    @brands = Brand.approved.where(:id => @shop.products.map(&:brand_id))

    @products = @shop.products
                          .by_search(params[:search])
                          .by_category(params[:category])
                          .by_subcategory(params[:subcategory])
                          .by_detail(params[:detail])
                          .by_brands(params[:brands])
                          .by_min_year(params[:min_year])
                          .by_max_year(params[:max_year])
                          .by_main_filter(params[:main_filter])
                          .by_sort(params[:sort_by])
                          .by_max_price(params[:max_price])
                          .page(params[:page])
                          .per(30)
  end

  def prepare_form
    @countries = ["Украина", "Россия", "Беларусь", "Казахстан", "Азербайджан", "Армения", "Грузия", "Израиль", "США", "Германия", "Латвия", "Литва", "Андорра", "Афганистан", "Албания", "Ангола", "Аргентина", "Австрия", "Австралия", "Азербайджан", "Босния и Герцеговина", "Барбадос", "Бангладеш", "Бельгия", "Буркина-Фасо", "Болгария", "Бахрейн", "Бурунди", "Бенин", "Сен-Бартелеми", "Боливия", "Бразилия", "Багамские Острова", "Бутан", "Ботсвана", "Белиз", "Канада", "Демократическая Республика Конго", "Центральноафриканская Республика", "Республика Конго", "Швейцария", "Кот-д’Ивуар", "Чили", "Камерун", "Китай", "Колумбия", "Коста-Рика", "Куба", "Кабо-Верде", "Кипр", "Чехия", "Джибути", "Дания", "Доминика", "Доминиканская Республика", "Алжир", "Эквадор", "Эстония", "Египет", "Западная Сахара", "Эритрея", "Испания", "Эфиопия", "Финляндия", "Франция", "Габон", "Великобритания", "Гренада", "Гана", "Гамбия", "Гвинея", "Экваториальная Гвинея", "Греция", "Гватемала", "Гвинея-Бисау", "Гайана", "Гондурас", "Хорватия", "Республика Гаити", "Венгрия", "Индонезия", "Ирландия", "Индия", "Ирак", "Иран", "Исландия", "Италия", "Джерси", "Иордания", "Япония", "Кения", "Киргизия", "Камбоджа", "Коморы", "Сент-Китс и Невис", "КНДР", "Республика Корея", "Кувейт", "Лаос", "Ливан", "Сент-Люсия", "Лихтенштейн", "Шри-Ланка", "Либерия", "Лесото", "Люксембург", "Ливия", "Марокко", "Монако", "Молдова", "Черногория", "Мадагаскар", "Македония", "Мали", "Мьянма", "Монголия", "Мавритания", "Мальта", "Маврикий", "Мальдивы", "Малави", "Мексика", "Малайзия", "Мозамбик", "Намибия", "Нигер", "Нигерия", "Никарагуа", "Нидерланды", "Норвегия", "Непал", "Новая Зеландия", "Оман", "Панама", "Перу", "Папуа — Новая Гвинея", "Филиппины", "Пакистан", "Польша", "Пуэрто-Рико", "Палестина", "Португалия", "Палау", "Парагвай", "Катар", "Румыния", "Сербия", "Руанда", "Саудовская Аравия", "Соломоновы Острова", "Сейшельские Острова", "Судан", "Швеция", "Сингапур", "Словения", "Словакия", "Сьерра-Леоне", "Сан-Марино", "Сенегал", "Сомали", "Суринам", "Южный Судан", "Сан-Томе и Принсипи", "Сальвадор", "Сирия", "Свазиленд", "Чад", "Того", "Таиланд", "Таджикистан", "Восточный Тимор", "Туркмения", "Тунис", "Турция", "Тринидад и Тобаго", "Танзания", "Уганда", "Уругвай", "Узбекистан", "Ватикан", "Сент-Винсент и Гренадины", "Венесуэла", "Вьетнам", "Вануату", "Йемен", "ЮАР", "Замбия", "Зимбабве"]
    @payment_types = PaymentType.all
  end

  def edit
    @shop = current_user.shop

    prepare_form
  end

  def update
    @shop = current_user.shop

    respond_to do |format|
      if @shop.update(shop_params)
        prepare_form
        set_countries
        flash[:notice] = 'Информация обновлена'
        format.html { render action: 'edit' }
        format.js { render 'success', layout: false }
      else
        prepare_form
        set_countries
        flash[:notice] = 'Информация не обновлена'
        format.html { render action: 'edit' }
        format.js { render 'error', layout: false }
      end
    end
  end

  def update_shop_cover
    if params['shop_cover'].present?
      current_user.shop.update(cover: params['shop_cover'])
    end
    render :text => current_user.shop.cover.url(:medium)
  end

  def get_cities
    if params['country_id']
      @country = params['country_id']
    elsif
      @country = "1"
    end
    require 'net/http'
    if params['q']
      sourceb = "http://api.vk.com/method/database.getCities?v=5.5&need_all=1&lang=ru&country_id=#{@country}&count=1000&q=#{params['q']}"
    else
      sourceb = "http://api.vk.com/method/database.getCities?v=5.5&need_all=1&lang=ru&country_id=#{@country}&count=1000"
    end
    encoded_url = URI.encode(sourceb)
    resp = Net::HTTP.get_response(URI.parse(encoded_url))
    datab = resp.body
    responseb = JSON.parse(datab)["response"]

    @cities_from_vk = responseb["items"].map {|x| x['title']}
    @cities_from_vk = @cities_from_vk.to_s
    respond_to do |format|
      format.js{ render 'get_cities', layout: false }
    end
  end


  def delete_shop_cover
    @shop = current_user.shop

    respond_to do |format|
      if @shop.update(cover:nil)
        format.html { redirect_to :back, notice: 'Изображение удалено ' }
        format.js {}
        format.json { :ok }
      end
    end
  end

  private

  def check_user
    @shop = Shop.friendly.find(params[:id])

    if @shop.user != current_user
      redirect_to root_path
    end
  end

  def set_countries
    require 'net/http'
    source = 'http://api.vk.com/method/database.getCountries?v=5.5&need_all=1&lang=ru0&count=1000'
    resp = Net::HTTP.get_response(URI.parse(source))
    data = resp.body
    response = JSON.parse(data)["response"]
    @countries_from_vk = response["items"]
  end

  def shop_params
    params.require(:shop).permit(:user_id, :title, :description, :cover, :site, :return,
      :delivery_method, :delivery_place, :formatted_address, :country, :state, :city, :lat, :lng,
      :payment_type_ids => [])
  end
end
