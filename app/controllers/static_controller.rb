class StaticController < ApplicationController
  def about
    @content = Content.find_by(:title => "about")
    set_seo(@content)
  end

  def partners
    @content = Content.find_by(:title => "partners")
    set_seo(@content)
  end

  def contacts
    @content = Content.find_by(:title => "contacts")
    set_seo(@content)
  end

  def faq
    @content = Content.find_by(:title => "faq")
    set_seo(@content)
  end

  def why_muzpoint
    @content = Content.find_by(:title => "why_muzpoint")
    set_seo(@content)
  end

  def how_advertising_works
    @content = Content.find_by(:title => "how_advertising_works")
    set_seo(@content)
  end

  def condition
    @content = Content.find_by(:title => "condition")
    set_seo(@content)
  end

  def terms_of_use
    @content = Content.find_by(:title => "terms_of_use")
    set_seo(@content)
  end

  def countries
    @content = Content.find_by(:title => "countries")
    set_seo(@content)
  end


end
