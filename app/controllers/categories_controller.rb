class CategoriesController < ApplicationController

  def show
    @category = Category.friendly.find(params[:id])

    if params[:page] == '1'
      redirect_to @category, :status => 301
    end

    @brands = Brand.where(id: @category.products.pluck(:brand_id)).take(12)
    @categories = Category.all.includes(:subcategories)

    @products = @category.products
                         .by_search(params[:search])
                         .by_category(params[:category])
                         .by_subcategory(params[:subcategory])
                         .by_detail(params[:detail])
                         .by_brands(params[:brands])
                         .by_min_year(params[:min_year])
                         .by_max_year(params[:max_year])
                         .by_main_filter(params[:main_filter])
                         .by_sort(params[:sort_by])
                         .by_max_price(params[:max_price])
                         .page(params[:page])
                         .per(30)

    @promo = @category.products
                      .joins(:promo_items)
                      .where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0)
                      .by_brands(params[:brands])
                      .sample(3)

    set_seo(@category)
  end
end
