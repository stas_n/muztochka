class BrandsController < ApplicationController

  def index
    if params[:alpha]
      @brands = Brand.approved.alpha(params[:alpha]).with_products.order('brands.title ASC')
    elsif params[:digit]
      @brands = Brand.order('title ASC').approved.where("title >= '0' and title < ':'")
    else
      @brands = Brand.order('title ASC').approved.where(id: Product.all.map(&:brand_id).uniq)
    end
    @content = Content.find_by(:title => "brands")
    set_seo(@content)
  end

  def show
    @brand = Brand.friendly.find(params[:id])

    @categories = Category.all

    @brands = Brand.approved
                   .find_by_sql("SELECT brands.*, COUNT(products.id) AS c FROM brands, products WHERE products.brand_id = brands.id GROUP BY brands.id ORDER BY c DESC")
                   .take(12)

    @products = @brand.products.includes(:photos)
                               .by_search(params[:search])
                               .by_category(params[:category])
                               .by_subcategory(params[:subcategory])
                               .by_detail(params[:detail])
                               .by_brands(params[:brands])
                               .by_min_year(params[:min_year])
                               .by_max_year(params[:max_year])
                               .by_main_filter(params[:main_filter])
                               .by_sort(params[:sort_by])
                               .by_max_price(params[:max_price])
                               .page(params[:page])
                               .per(30)

    @promo = @brand.products
                   .includes(:photos)
                   .joins(:promo_items)
                   .where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0)
                   .by_category(params[:category])
                   .by_brands(params[:brands])
                   .sample(3)

    set_seo(@brand)
  end
end
