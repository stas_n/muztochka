class CountriesController < ApplicationController

  def index
    @countries = Product.reorder('country DESC').uniq.pluck(:country)
    @content = Content.find_by(:title => "countries")

    set_seo(@content)
  end

  def show
    @geo = Country.find_by(:permalink => params[:id])

    if params[:page] == '1'
      redirect_to country_path(@geo.permalink), :status => 301
    end

    @categories = Category.all
    @brands = Brand.approved
                   .popular
                   .take(12)

    @products = Product.where(country: @geo.name)
                       .by_search(params[:search])
                       .by_category(params[:category])
                       .by_subcategory(params[:subcategory])
                       .by_detail(params[:detail])
                       .by_brands(params[:brands])
                       .by_min_year(params[:min_year])
                       .by_max_year(params[:max_year])
                       .by_main_filter(params[:main_filter])
                       .by_sort(params[:sort_by])
                       .by_max_price(params[:max_price])
                       .page(params[:page])
                       .per(30)

    @promo = Product.where(country: @geo.name)
                    .joins(:promo_items)
                    .where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0)
                    .by_category(params[:category])
                    .sample(3)

    set_seo(@geo)
  end
end
