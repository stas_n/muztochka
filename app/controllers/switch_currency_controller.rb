class SwitchCurrencyController < ApplicationController

  def switch
    params[:currency] ? cookies[:user_currency] = params[:currency] : cookies[:user_currency]
    redirect_to :back
  end

end