class ChatWebsocketController < WebsocketRails::BaseController
  def initialize_session
  end

  def new_message
    @message = Message.create(
      conversation_id: message[:conversation_id],
      sender: current_user,
      text: message[:text],
      recipient_id: message[:recipient]
    )
    WebsocketRails[@message.conversation.id.to_s].trigger :message_added,
     view_context.render(partial: chat_partial,
      locals: {message: @message}, layout: false)
  end

  private

  def chat_partial
    if mobile_version?
      'm/conversations/message'
    else
      'conversations/message'
    end
  end

end
