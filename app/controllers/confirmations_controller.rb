class ConfirmationsController < Devise::ConfirmationsController

  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?

    if resource.errors.empty?
#      set_flash_message(:notice, :confirmed) if is_flashing_format?
#      respond_with_navigational(resource){ redirect_to root_path(:user => resource.id, :set_password => true) }

      sign_in(resource)
      respond_with_navigational(resource){ redirect_to root_path(set_password: DateTime.now.to_i) }
    else
      respond_with_navigational(resource.errors, status: :unprocessable_entity){ render :new }
    end
  end
end
