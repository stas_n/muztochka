class ConversationsController < ApplicationController
  before_filter :sign_in_from_email

  def index
    @conversations = Conversation.where('sender_id = ? OR recipient_id = ?', current_user.id, current_user.id)
    @conversations = @conversations.where('(sender_id = ? AND show_for_sender = ?) OR (recipient_id = ? AND show_for_recipient = ?)', current_user.id, true, current_user.id, true)
    @outbox_messages = current_user.messages.where(sender_id: current_user.id)
    @inbox_messages = current_user.messages.where('messages.sender_id != ?',current_user.id)
    if params[:sentence].present?
      ids = @conversations.pluck(:id)
      cids = Message.where(conversation_id: ids).includes(:sender).
          where('messages.text ILIKE ? OR users.name ILIKE ?',
                '%' + params[:sentence] + '%',
                '%' + params[:sentence] + '%').pluck(:conversation_id)
      @conversations = @conversations.where(id: cids).page(params[:page]).per(30)
    end
  end

  def create
    @conversation = Conversation.find_or_create_by(sender_id:current_user.id, recipient_id:params[:user_id],product_id:params[:product_id])
    redirect_to @conversation
  end

  def show
    @conversation = Conversation.find(params[:id])
    gon.channel = @conversation.id.to_s
    @conversation.update_check(current_user)
    # do we need this?
    @conversation_messages = @conversation.messages.order('created_at asc')
    gon.websocketpath = Rails.env.production? ? 'muztochka.com/websocket' : (request.host_with_port + '/websocket')
    if Rails.env.production?
      gon.websocketpath = 'muztochka.com/websocket'
    elsif Rails.env.staging?
      gon.websocketpath = 'muzdot.thedigitalcrafters.com/websocket'
    else
      gon.websocketpath = request.host_with_port + '/websocket'
    end
  end

  def destroy
    @conversation = Conversation.find(params[:id])
    if @conversation.sender_id == current_user.id
      @conversation.update(show_for_sender: false)
    elsif @conversation.recipient_id == current_user.id
      @conversation.update(show_for_recipient: false)
    end
    redirect_to conversations_path
  end

  private

  def sign_in_from_email
    if params[:recipient_id]
      user = User.find(params[:recipient_id])
      if user
        sign_in user, :bypass => true
        redirect_to conversation_path(params[:id])
      else
        redirect_to root_path
      end
    end
  end

end
