class MessagesController < ApplicationController


  def mark_as_read
    messages = Message.unread_messages(current_user)
    messages.each {|m|m.mark_as_read! :for => current_user}
    render json: :ok
  end
end