class SessionsController < Devise::SessionsController
  def create
    respond_to do |format|
      format.js do
        self.resource = warden.authenticate(auth_options)
        if resource && resource.active_for_authentication?
          sign_in(resource_name, resource)
        end
        render layout: false
      end
    end
  end
end
