class ErrorsController < ApplicationController

  skip_before_filter :set_new_message_count
  skip_before_filter :set_feedbacks
  skip_before_filter :set_flash
  skip_before_filter :check_mobile
  skip_before_filter :check_current_layout
  skip_before_filter :set_default_user_currency

  def page_not_found
    respond_to do |format|
      format.html { render template: 'errors/404', status: 404 }
      format.all  { render nothing: true, status: 404 }
    end
  end

  def server_error
    respond_to do |format|
      format.html { render template: 'errors/500', status: 500 }
      format.all  { render nothing: true, status: 500}
    end
  end

end
