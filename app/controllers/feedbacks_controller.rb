class FeedbacksController < ApplicationController

  def create
    @feedback = Feedback.new(feedback_params)
    respond_to do |format|
      if @feedback.save
        format.js { render 'success', layout: false }
      else
        format.js { render 'error', layout: false  }
      end
    end
  end


private

  def feedback_params
    params.require(:feedback).permit(:name, :email, :phone, :text)
  end
end
