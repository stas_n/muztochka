class HomeController < ApplicationController
  def index
    if params[:set_password].present? && current_user.present?
      @user = current_user
    end

    if params[:page] == '1'
      redirect_to root_path, :status => 301
    end

    @categories = Category.all.includes(:subcategories,:details)

    @brands = Brand.approved
                   .popular
                   .take(12)

    @products = Product.includes(:photos)
                       .by_search(params[:search])
                       .by_category(params[:category])
                       .by_subcategory(params[:subcategory])
                       .by_detail(params[:detail])
                       .by_brands(params[:brands])
                       .by_min_year(params[:min_year])
                       .by_max_year(params[:max_year])
                       .by_main_filter(params[:main_filter])
                       .by_sort(params[:sort_by])
                       .by_max_price(by_currency(params[:max_price]))
                       .page(params[:page])
                       .per(30)



    mobile_version? ? premium(2) : premium(3)

    @content = Content.find_by(:title => "home")
    set_seo(@content)
  end


  private


  def premium(num)
    @promo = Product.joins(:promo_items)
                    .where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0)
                    .includes(:photos)
                    .by_brands(params[:brands])
                    .sample(num)
  end

  def by_currency(price)
    if price.to_i > 0
      return price if cookies[:user_currency] == 'usd'
      from = cookies[:user_currency]
      send("#{from}_to_usd", price.to_f) #CurrencyExchange module:58
    end
  end
end
