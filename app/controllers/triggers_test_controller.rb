class TriggersTestController < ApplicationController
  def triggers_test
    product = Product.offset(rand(Product.count)).first
    shop = product.shop
    user = product.shop.user
    email = params[:email] || 'muzdot@mailinator.com'
    #conversation
    case params[:type]
    when 'advantages'
      advantages_trigger_test(email)
    when 'products'
      products_trigger_test(email, user)
    when 'configure'
      configure_trigger_test(email, shop, user)
    when 'message_sent'
      message_sent_trigger_test(email, user, shop)
    when 'message_received'
      message_received_trigger_test(email, user, shop)
    when 'favorites'
      favorites_trigger_test(email, product, user)
    else
      render :text => "No trigger type specified.<br>Available options are: 'advantages', 'products', 'configure, 'message_sent', 'message_received', 'favorites'.<br>emails for testing: muzdot_1@mailinator.com and muzdot_2@mailinator.com"
    end
  end

  private

  def advantages_trigger_test(email)
    EmailTriggersMailer.welcome_email_advantages(email).deliver
    render 'mailer/welcome_advantages', layout: false
  end

  def products_trigger_test(email,user)
    @user_id = user.id
    EmailTriggersMailer.welcome_email_products(email, user.id).deliver
    render 'mailer/welcome_products', layout: false
  end

  def configure_trigger_test(email, shop, user)
    @slug = shop.slug
    @user_id = user.id
    EmailTriggersMailer.welcome_email_configure(email, shop.slug, user.id).deliver
    render 'mailer/welcome_configure', layout: false
  end

  def message_sent_trigger_test(email, user, shop)
    @shop_name = user.name
    @slug = shop.slug
    EmailTriggersMailer.message_sent_email(email, user.name, shop.slug).deliver
    render 'mailer/message_sent', layout: false
  end

  def message_received_trigger_test(email, user, shop)
    conversation_id = Conversation.offset(rand(Conversation.count)).first.id
    @shop_name = user.name
    @slug = shop.slug
    @conversation_id = conversation_id
    EmailTriggersMailer.message_received_email(email, user.name, shop.slug, conversation_id).deliver
    render 'mailer/message_received', layout: false
  end

  def favorites_trigger_test(email, product, user)
    @product_title = product.title
    @product_slug = product.slug
    @product_owner_id = user.id
    EmailTriggersMailer.favorites_add_email(email, product.title, product.slug, user.id).deliver
    render 'mailer/favorites_add', layout: false
  end

end
