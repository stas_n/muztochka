class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  before_filter :set_new_message_count
  before_filter :set_feedbacks
  before_filter :set_flash
  before_filter :check_mobile, unless: -> { request.url.include?('admin') }
  before_action :check_current_layout
  before_filter :set_default_user_currency

  def set_new_message_count
    @new_messages = current_user.count_all_unread_messages if current_user
  end

  def set_feedbacks
    @footer_subscriber = Subscriber.new
    @footer_feedback = Feedback.new
  end

  def set_flash
    flash[:notice] = ''
  end


  def set_seo(page)
    set_meta_tags :site => '', :title => page.seo_title ? page.seo_title : '', :reverse => true,
                  :description => page.seo_description ? page.seo_description : "",
                  :keywords => page.seo_keywords ? page.seo_keywords : ""
  end

  def check_mobile
    if mobile_version?
      self.class.layout 'mobile'
      prepend_view_path Rails.root + 'app' + 'views' + 'm'
    else
      self.class.layout 'application'
      prepend_view_path Rails.root + 'app' + 'views'
    end
  end

  def mobile_version?
    check_subdomain || check_browser
  end

  helper_method :mobile_version?


  def access_denied(exception)
    redirect_to  root_path, :alert => exception.message
  end

  def after_sign_in_path_for(resource) # admin_login
    if resource_role(resource,:admin)
      admin_root_path
    elsif resource_role(resource,:moderator)
      admin_posts_path
    else
      root_path
    end
  end


  def set_default_user_currency
    cookies[:user_currency] = 'usd' if cookies[:user_currency].nil?
  end

  private

  def check_subdomain
    subdomains = %w(m mobile)
    request.host.split('.').first.in?(subdomains)
  end

  def check_browser
    Browser.new(request.user_agent).device.mobile?
  end

  def check_current_layout
    @current_layout = _layout
  end

  def resource_role(resource,role)
    resource.class == AdminUser && resource.has_role?(role)
  end
end
