class CitiesController < ApplicationController

  def index

    @cities = Product.reorder('city ASC').uniq.pluck(:city, :state, :country)

    if params[:alpha]
      @cities = Product.alpha(params[:alpha]).pluck(:city, :state, :country).uniq
    end

    if params[:country]
      @cities = Product.where(:country => params[:country]).reorder('city ASC').uniq.pluck(:city, :state, :country)
    end

    @content = Content.find_by(:title => "cities")
    set_seo(@content)
  end

  def show
    @geo = City.find_by(:permalink => params[:id], :permalink_state => params[:state_id])

    if params[:page] == '1'
      redirect_to city_path(permalink: @geo.permalink, permalink_state: @geo.permalink), :status => 301
    end

    @categories = Category.all

    @brands = Brand.approved
                   .popular
                   .take(12)

    @products = Product.where(state: @geo.state, city: @geo.name)
                       .by_search(params[:search])
                       .by_category(params[:category])
                       .by_subcategory(params[:subcategory])
                       .by_detail(params[:detail])
                       .by_brands(params[:brands])
                       .by_min_year(params[:min_year])
                       .by_max_year(params[:max_year])
                       .by_main_filter(params[:main_filter])
                       .by_sort(params[:sort_by])
                       .by_max_price(params[:max_price])
                       .page(params[:page])
                       .per(30)

    @promo = Product.where(state: @geo.state, city: @geo.name)
                    .joins(:promo_items)
                    .where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0)
                    .by_category(params[:category])
                    .sample(3)

    set_seo(@geo)
  end
end
