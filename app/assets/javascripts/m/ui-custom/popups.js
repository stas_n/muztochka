$(document).ready(function () {

    $('.sort_active').click(function () {
        $('#sort').slideDown();
        $('body').addClass('popup_showed');
    });

    $('.promote').click(function () {
        var productId = $(this).data('product-id');
        $('#bump-' + productId).slideDown();
        $('body').addClass('popup_showed');
    });

    $('.back_profile').click(function () {
        $('body').removeClass('popup_showed');
        $('.popup:visible').slideUp();
    });

    $('.del_dialog_open').click(function () {
        $('.del_dialog').slideDown();
    });

    $('.cancel_del').click(function () {
        $('.del_dialog').slideUp();
        $('.del_dialog2').slideUp();
    });

    $('.open_feedback').click(function () {
        $('.popup_mesage').slideDown(100);
    });

    $('.open_currency').click(function () {
        $('.popup_currency').slideDown(100);
    });

    $('.popup_mesage .close').click(function () {
        $('.popup_mesage').slideUp(100);
        $('#message-sent').hide();
    });

    $('.popup_currency .close').click(function () {
        $('.popup_currency').slideUp(100);
    });

    $('.currency').click(function(){
        $('.active_currency').removeClass('active_currency')
    })

});
