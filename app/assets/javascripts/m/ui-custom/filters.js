$(document).ready(function () {

  $('.open_filtr').click(function () { //open main filter's window
    $('.popup_filtr, .filtr_wraper').slideDown();
    $('body').addClass('popup_showed');
    return false;
  });

  $('#sort.js-sort-filters').click(function () {  //dropdown
    $('.drop_down_content').slideToggle();
    $('#sort input').toggleClass('active');
    return false;
  });

  $('#sort .drop_down_content a').click(function () { //dropdown
    console.log($(this).attr('data-sort'));
    $('#sort_by').val($(this).attr('data-sort'));
    $('#sort input').val($(this).text());
    $('.drop_down_content').slideUp(200);
    $('#sort input').removeClass('active');
    return false;
  });


  $('#cat_open').click(function () {
    $('#cat_form').addClass('active');
    $('.filtr_wraper').addClass('active');
    return false;
  });

  $('#years_open').click(function () {
    $('#years_form').addClass('active');
    $('.filtr_wraper').addClass('active');
    return false;
  });

  $('#brands_open').click(function () {
    $('#brands_form').addClass('active');
    $('.filtr_wraper').addClass('active');
    return false;
  });

  $('#options_open').click(function () {
    $('#options_form').addClass('active');
    $('.filtr_wraper').addClass('active');
    return false;
  });


  $('#years_form, #cat_form, #brands_form, #options_form').on('click', '.back', function(event) {
    //$('.brend_opc_wraper.active input[type="checkbox"]').attr('checked',false);
    backF ();
  });

  function backF () {
    $('#years_form, #cat_form, #brands_form, #options_form, .filtr_wraper').removeClass('active');
    return false;
  }

  $('#close_filter').click(function() {
    $('.popup_filtr, .filtr_wraper').slideUp(200);
    $('body').removeClass('popup_showed');
    return false;
  });

  $('.allBrends').click(function() {
    $('.brend_opc_wraper.active input[type="checkbox"]').prop('checked',true);
  });

  $('a[data-cat]').click(function () {
    var catId = $(this).data('category-id');
    $('#cat_input').val($(this).attr('data-cat'));
    $('#category').val(catId);
    if ( parseInt(catId) != 16 ) {
      $('#options_open').show();
      $('#options_form .list[data-parent-category-id=' + catId + ']').show().siblings('.list').hide();
    } else {
      $('#options_open').hide();
      $('#subcategory,#detail').val('');
    }
    backF ();
  });

  $('#years_form .list a').click(function () {
    $('#min_year').val($(this).data('min'));
    $('#max_year').val($(this).data('max'));
    $('#year_input').val($(this).text());
    backF ();
  });

  $('#add_brens').click(function() {
    backF ();
    return false;
  });

  $('#add_options').click(function() {
    var tempInput='', tempText='';
    $('#options_form input:checked').each(function (e) {
      tempText=tempText+$(this).attr('data-options')+', ';
      tempInput=tempInput+$(this).attr('data-options')+'✓';
    })
    $('#options_open input').val(tempInput);
    if(tempText === '')tempText='<div class="placeholder">Выберите опцию</div>'
    $('#options_open .text').html(tempText);
    backF ();
    return false;
  });

  $('#options_form .list a').click(function () {
    $('#subcategory').val($(this).data('subcategory-id'));
    $('#detail').val($(this).data('detail-id'));
    $('#options_open .text').html($(this).text());
    backF ();
    return false;
  });

  $('body').on('click', '.seach_res a, .seach_res_2 a', function() {
    var inputId = $(this).data('input-id'),
        inputType = $(inputId).attr('type');
    if ( inputType == 'radio' || inputType == 'checkbox' )
      $(inputId)[0].checked = false;
    else
      $(inputId).val('');
    $(inputId).closest('form').submit();
    return false;
  });

});
