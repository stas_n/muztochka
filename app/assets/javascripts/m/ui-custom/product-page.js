$(document).ready(function () {

  $('.prod_info .description').click(function() {

    $(this).addClass('active');
    $('.prod_info .cound').removeClass('active');

    $('.prod_info .prod_descr').slideDown(300);
    $('.prod_info .prod_descr_cound').slideUp(300);
    return false;
  });

  $('.prod_info .cound').click(function() {

    $(this).addClass('active');
    $('.prod_info .description').removeClass('active');

    $('.prod_info .prod_descr_cound').slideDown(300);
    $('.prod_info .prod_descr').slideUp(300);
    return false;
  });

});