$(document).ready(function () {
  var reg_login = $('#reg_login');
  var reg_registration = $('#reg_registration');
  var login_link = $('.login_link');

  $('#reg_login_show').slideUp();

  if(reg_login.length > 0){
     $('#reg_login, .login_link').click( function (e) {
        $(reg_login).addClass('active');
        $('#login_soc').addClass('active');
        $(reg_registration).removeClass('active');
        $('#add_soc').removeClass('active');

        $('#reg_registration_show').slideDown();
        $('#reg_login_show').slideUp();
        return false;
     });
  };

  if(reg_registration.length > 0){
     $('#reg_registration, .registr_link').click( function (e) {

        $(reg_registration).addClass('active');
        $('#add_soc').addClass('active');
        $(reg_login).removeClass('active');
        $('#login_soc').removeClass('active');

        $('#reg_registration_show').slideUp();
        $('#reg_login_show').slideDown();
        return false;
     });
  };

  $('body').on('click', '.notification-close', function() {
    $(this).closest('.notification').fadeOut('300',function () {
      $(this).removeClass('active');
    });
    return false;
  });


    function registration_tab_active() {
        reg_login.removeClass('active');
        reg_registration.addClass('active');
    }

    if (document.referrer.includes('users/password/new')) {
        registration_tab_active()
    }

    $('#m_reg').on('click', function () {
        registration_tab_active();
        $('#open_top_menu').toggleClass('active');
        $('#account_block_top').slideToggle();
        localStorage.setItem("registration", "new_user");
    });

    if (localStorage.registration === 'new_user') {
        registration_tab_active();
    }

    $('#m_login').on('click', function () {
        localStorage.removeItem("registration");
    });


});