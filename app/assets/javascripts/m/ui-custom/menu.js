$(document).ready(function () {
  var open_menu = $('.content #open_menu');
  if(open_menu.length){
    $('body').on('click', '.content #open_menu', function (e) {
      //$('.category_menu_block').removeClass('active');
      $(this).parents('.category_menu_block').toggleClass('active').find('.menu_show_section').toggle('300');
      return true;
    });
  };

  $('#account_open').click(function() {
    $(this).toggleClass('active');
    $('#account_menu').slideToggle();
    return false;
  });

  $('#open_top_menu').click(function() {
    $(this).toggleClass('active');
    $('#account_block_top').slideToggle();
    return false;
  });

});
