$ ->
  isLocationValid = false
  #editor = CKEDITOR.instances.product_description if CKEDITOR?

  # Product.new form custom validation
  $('#new_product button[type=submit]').on 'click', ->
    tpl = $('<span>').addClass 'help-block form-error'

    # Validating custom dropdowns with attr [required]
    $('.dropdown[required]:not(.has-error) .js-link:visible').each ->
      if $(@).is ':contains(Выберите)'
        msg = $(@).parent().data 'validation-msg'
        $(@).parent().addClass('has-error').append tpl.clone().text msg

    # Validating custom file upload
    if $('.dragdrop-item').length is 0
      msg = $('.dragdrop-section').data 'validation-msg'
      $('.dragdrop-section:not(.has-error)').addClass('has-error').append tpl.clone().text msg

    # Additional validation for location input
    if isLocationValid is false
      msg = $('#formatted_address').data 'validation-error-msg'
      unless $('#formatted_address').parent().hasClass 'has-error'
        $('#formatted_address').parent().addClass('has-error').append tpl.clone().text msg

    # Validate presence of product_description
    #if isCKEditorBlank(editor.getData())
      #msg = $('#product_description').data 'validation-error-msg'
      #$('#product_description').parent().addClass('has-error').append tpl.clone().text msg

    isNotValid = $('.dropdown.has-error').length > 0 or
                 $('.dragdrop-section.has-error').length > 0 or
                 isLocationValid is false# or
                 #isCKEditorBlank(editor.getData())

    $(@).trigger('submit') # Manualy trigger form validation
    return false if isNotValid
    return $(@).closest('form').submit()

  # Remove .has-error class from valid custom dropdowns
  # and remove error message
  $('body').on 'click', '.dropdown.has-error li', ->
    $(@).closest('.dropdown').removeClass('has-error').find('.form-error').remove()

  # Remove .has-error class from valid custom file upload
  $('body').on 'fileuploadadd', ->
    $('.dragdrop-section.has-error').removeClass('has-error').find('.form-error').remove()

  # Remove .has-error class from valid location input
  $('body').on 'keydown', '#formatted_address', ->
    isLocationValid = false
    return true

  $('body').on 'geocode:result', (e, result) ->
    $formattedAddress = $('input[data-geo=formatted_address]')
    $formattedAddress.val cutPostalCode result.formatted_address
    isLocationValid = true
    if isLocationValid
      $('#formatted_address').parent('.has-error').removeClass('has-error').find('.form-error').remove()

  # Check CKEDITOR if blank
  #isCKEditorBlank = (data) ->
    #div = $('<div/>').html(data)[0]
    #text = div.textContent || div.innerText
    #if $.trim(text).length is 0 then true else false

  #if editor?
    #editor.on 'change', (e) ->
      #if not isCKEditorBlank(e.editor.getData())
        #$('#product_description').parent('.has-error').removeClass('has-error').find('.form-error').remove()

  cutPostalCode = (str) ->
    rx = new RegExp /(,\s)?[0-9]+/g
    return str = str.replace rx, ''
