# Init validation on blur
$ ->
  $.validate
    form: '.js-form-validation'
    modules: 'html5, security'
    validateOnBlur: true
    borderColorOnError: false

# Init validation on submit
$ ->
  $.validate
    form: '.js-form-validation-onsubmit'
    modules: 'html5, security'
    validateOnBlur: false
    borderColorOnError: false
