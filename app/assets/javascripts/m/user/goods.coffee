$ ->
  $('.popover-content').on 'mouseleave', ->
    setTimeout (->
      $('.popover-content').slideUp(200)
      return
    ), 200

  $('.js_status_ckeckbox').on 'change', ->
    $(@).closest("form").submit()
