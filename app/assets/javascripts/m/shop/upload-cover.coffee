$ ->
  $('#shop_cover').on 'change', ->
    formData = new FormData()
    fileType = $(@)[0].files[0].type
    fileSize = $(@)[0].files[0].size
    allowdtypes = $(@).prop('accept')

    if allowdtypes.indexOf(fileType) < 0
      alert 'Недопустимый формат изображения'
      $(@).val ''
      return false

    if fileSize > (5 * 1024 * 1024)
      alert 'Размер изображения не должен превышать 5Мб'
      $(@).val ''
      return false

    formData.append 'shop_cover', $(@)[0].files[0]
    $.ajax
      type: 'post'
      url: '/shops/update_shop_cover'
      data: formData
      contentType: false
      processData: false
    .done (resp) ->
      $(@).val ''
      $('.img_cover img').attr src: resp
    .fail (resp) ->
      $(@).val ''
      alert 'Чтото пошло не так, попробуйте повторить чуть позже'
    .always (resp) ->
      $(@).val ''
