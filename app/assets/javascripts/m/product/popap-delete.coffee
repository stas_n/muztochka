$ ->
  $('body').on 'click', '.product-delete', ->
    product_id = $(this).attr('data-id')
    $('#product-delete').slideDown();
    $('#product-delete form').attr('action', '/products/' + product_id)
    false

  $('body').on 'change', '#product-delete input', (e) ->
    $(@).closest('form').submit();
