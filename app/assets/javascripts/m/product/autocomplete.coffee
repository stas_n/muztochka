$ ->
  if gon?
    if gon.brands?
      $("#product_brand").autocomplete
        source: gon.brands
        messages:
          noResults: '',
          results: -> ''
    if gon.producing_countries?
      $("#product_country_producing").autocomplete
        source: gon.producing_countries
        messages:
          noResults: '',
          results: -> ''
