$ ->
  files_count = 0
  files_uploaded = 0
  FILES_LIMIT = 16
  MAX_FILE_SIZE = 5 * 1024 * 1024 # 5Mb
  $fileErrorsMime = $('.file-errors-mime .file-errors-list')
  $fileErrorsSize = $('.file-errors-size .file-errors-list')
  $fileErrorsMax = $('.file-errors-max')
  acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i

  $(document).bind 'drop dragover', (e) ->
    e.preventDefault()

  $('#image_input').fileupload(
    dropZone: $('#dropzone')
    sequentialUploads: true
    limitConcurrentUploads: 1
    acceptFileTypes: acceptFileTypes
  )
  .on('fileuploaddone', (e) ->
    files_uploaded++
    $('.foto_dowload')
      .removeClass('dragdrop_state_begin')
      .addClass('dragdrop_state_photos')
    $('.dragdrop__progress')
      .removeClass('dragdrop__progress_state_finish')
      .addClass('dragdrop__progress_state_loading')

    progress = (files_uploaded / files_count) * 100
    $('.progress-bar').css('width', progress + '%')
    if files_uploaded == files_count
      initPhotosSortable()
      #$('.progress-bar').css('width', '0%')
      $('.dragdrop__progress').addClass('dragdrop__progress_state_finish')
  )
  .on 'fileuploadsubmit', (e, data) ->
    # Hack for product edit form
    if window.initProductEdit
      files_count = window.files_count
      files_uploaded = window.files_count
      window.initProductEdit = false

    file = data.files[0]

    if files_count >= FILES_LIMIT
      $fileErrorsMax.show()
      return false

    unless acceptFileTypes.test file.type
      $fileErrorsMime
        .append $('<span>').text file.name
        .parent().show()
      return false

    if file.size > MAX_FILE_SIZE and acceptFileTypes.test file.type
      $fileErrorsSize
        .append $('<span>').text file.name
        .parent().show()
      return false

    files_count++

  $('body').on 'click', '.dragdrop__text', ->
    $('#image_input').trigger 'click'
    resetFileErrors()

  $('body').on 'click', '.dragdrop-item__remove a.link-grey', ->
    id  = $(@).attr('data-id')
    $(@).parents('.dragdrop-item').remove()
    $("input.photo-ids[value='" + id + "']").remove()
    $('input.photo-position.photo-' + id).remove()
    --files_count
    --files_uploaded
    resetFileErrors()
    false

  initPhotosSortable = ->
    $('.dragdrop-items').sortable update: (event, ui) ->
      i = 0
      while i <= $('.dragdrop-item').length
        id = $('.dragdrop-item').eq(i).attr('data-id')
        $('input.photo-' + id).val i
        i++

  resetFileErrors = ->
    $('.file-errors > div').hide()
    $('.file-errors > div span').empty()

  initPhotosSortable()
