$ ->
  $('.js-form-submit').click (e) ->
    e.preventDefault()
    $(@).closest('form').submit()

  $('[data-toggle="tooltip"]').tooltip()

  $('#all-categories-toggle').on 'click', ->
    $(@).toggleClass 'js-active'
    $('.all-categories-section').slideToggle()



  $('#unread_msgs').click ->
    $.ajax
      type: 'POST',
      url: '/mark_as_read',
      success: 200