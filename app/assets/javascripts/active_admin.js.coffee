#= require active_admin/base

$(document).ready ->
  if $('#post_first_paragraph_title').length > 0
    CKEDITOR.replace 'post_first_paragraph_title',
      toolbar :
        [
          { name: 'links', items: [ 'Link','Unlink' ] }
        ]


$(document).ready ->
  if window.location.pathname == "/admin/contents/home/edit"
    editor = CKEDITOR.instances.content_description
    editor.destroy(true)
    CKEDITOR.replace 'content_description',
      toolbar : 'Full'

#edit product sub cat
$(document).ready ->
  category_id = $('#product_category_id').attr('selected', 'selected').val()
  product_id = $('#product_id').val()
  if undefined != category_id && category_id.length && undefined != product_id && product_id.length

    $.ajax
      type: 'GET'
      url: "/products/dynamic_admin_subcategory?category_id=#{category_id}&id=#{product_id}"
      dataType: 'json'
      success: (data) ->
        $('#product_subcategory_id').empty()
        subcat = $('#product_subcategory_id')
        $.each data, (value, key) ->
          $("<option />", {value: key.id, text: key.title}).appendTo(subcat);
          $("#product_subcategory_id option[value=#{key.selected}]").prop("selected", "selected")
        return
  return


#new product sub cat
$(document).ready ->
  $('#product_category_input').change ->
    category_id = $('#product_category_id').attr('selected', 'selected').val()
    $.ajax
      type: 'GET'
      url: '/products/dynamic_admin_subcategory?category_id=' + category_id
      dataType: 'json'
      success: (data) ->
        $('#product_subcategory_id').empty()
        subcat= $('#product_subcategory_id')
        $.each data, (value, key) ->
          $("<option />", {value: key[0], text: key[1]}).appendTo(subcat);

        return
    return
  return
return

