$ ->
  $('#shop_country_id').on 'change', ->
    $('#shop_cit').show()
    $.ajax(
      url: '/shops/get_cities'
      method: "POST"
      dataType: 'script'
      data: {country_id: $('#shop_country_id').val()}
    ).done (data)->
    $('#city_drop').html(data)
    return
  $("#shop_city").on 'change', ->
    $.ajax(
      url: '/shops/get_cities'
      method: "POST"
      dataType: 'script'
      data:
        country_id: $('#shop_country_id').val()
        q: $("#shop_city").val()
    ).done (data)->
    $('#city_drop').html(data)
    return