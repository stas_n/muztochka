$ ->
  $('#shop_filters_list a').on 'click', ->
    $('#category').val($(this).attr('data_id'))
    $('#subcategory').val($(this).attr('sub_data_id'))
    $('#detail').val($(this).attr('det_data_id'))
    $('#shop_filters_form').submit()
    false


  $('#shop_list_years a').on 'click', ->
    $('#min_year').val($(this).attr('data-min'))
    $('#max_year').val($(this).attr('data-max'))
    $('#shop_filters_form').submit()
    false

  $('.check__shop_form').on 'change', ->
    $('#shop_filters_form').submit()

  $('#shop_filtering__buttons a').on 'click', ->
    $('#main_filter').val($(@).attr('data-type'))
    $('#shop_filters_form').submit()
    false

  $('#submit_shop_filter_form').on 'click', ->
    $('#shop_filters_form').submit()
    false
