$ ->
  if $('.form_profile').length == 0
    return false
  $('#avatar_button').on 'click', ->
    $('#user_avatar').trigger 'click'
    false
  $('#submit').on 'click', (e)->
    $('.form_profile').submit()
    false
  true
  $('.promo-button-js').on 'click', ->
  	$(@).parent().find('.popover-content').show()
  	false
  $('.ios-check__checkbox').on 'change', ->
    window.location.href = $(@).parents('a').attr('href')

  $('.field_with_errors input').keydown ->
    $(@).parents('.inputbox').find('.inputbox__error').remove()

  $('#user_avatar').on 'change', ->
    file = $(@)[0].files[0]
    fileSize = file.size
    fileType = file.type
    accept = $(@).prop 'accept'

    if (accept.indexOf(fileType) is -1) or not fileType
      alert 'Неверный формат изображения, допустимые форматы jpeg,png,gif'
      $(@).val ''
      return false

    if fileSize > (5 * 1024 * 1024) # If file size > 5Mb
      alert 'Слишком большой размер изображения, изображение должно быть не более 5Мб'
      $(@).val ''
      return false

    $('form').submit()
