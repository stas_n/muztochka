$ ->
  $('#details-list a').on 'click', ->
    category = $(this).attr('data_id')
    $('#filter_form').attr('action', '/categories/' + category)
    $('#subcategory').val($(this).attr('sub_data_id'))
    $('#detail').val($(this).attr('det_data_id'))
    $('#filter_form').submit()
    false

  $('.check__form').on 'change', ->
    $('#filter_form').submit()

  $('#all_brands').on 'click', ->
    $('.checkbox__input').prop('checked', false)
    $('#filter_form').submit()
    false

  $('.filter-list_years a').on 'click', ->
    $('#min_year').val($(this).attr('data-min'))
    $('#max_year').val($(this).attr('data-max'))
    $('#filter_form').submit()
    false

  $('#all_years').on 'click', ->
    $('#min_year').val("")
    $('#max_year').val("")
    $('#filter_form').submit()
    false

  $('.page-filtering.filtering__buttons a').on 'click', ->
    $('#main_filter').val($(@).attr('data-type'))
    $('#filter_form').submit()
    false
  $('.filtering__buttons a').on 'click', ->
    $('#main_filter').val($(@).attr('data-type'))
    $('#filter_form').submit()
    false

  $('#submit_filter_form').on 'click', ->
    $('#filter_form').submit()
    false
