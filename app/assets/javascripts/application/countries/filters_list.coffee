$ ->
  $('#country_filters_list a').on 'click', ->
    $('#category').val($(this).attr('data_id'))
    $('#subcategory').val($(this).attr('sub_data_id'))
    $('#detail').val($(this).attr('det_data_id'))
    $('#country_filters_form').submit()
    false

  $('#country_list_years a').on 'click', ->
    $('#min_year').val($(this).attr('data-min'))
    $('#max_year').val($(this).attr('data-max'))
    $('#country_filters_form').submit()
    false

  $('#country_filtering__buttons a').on 'click', ->
    $('#main_filter').val($(@).attr('data-type'))
    $('#country_filters_form').submit()
    false

  $('#submit_filter_form').on 'click', ->
    $('#country_filters_form').submit()
    false

  $('.check_country_form').on 'change', ->
    $('#country_filters_form').submit()


  $('body').on 'click', '#brand_params a', ->
    brand = $(this).attr('brand-id')
    $('#checkbox_' + brand).trigger 'click'
    false


  $('#country_main_submit').on 'click', ->
    $('#main_filter').val('')
    $('#country_filters_form').submit()
    false

  $('#country_subcat_submit').on 'click', ->
    $('#subcategory').val('')
    $('#country_filters_form').submit()
    false

  $('#country_detail_submit').on 'click', ->
    $('#detail').val('')
    $('#country_filters_form').submit()
    false

  $('#country_max_year_submit').on 'click', ->
    $('#max_year').val('')
    $('#country_filters_form').submit()
    false

  $('#country_min_year_submit').on 'click', ->
    $('#min_year').val('')
    $('#country_filters_form').submit()
    false

  $('#country_price_submit').on 'click', ->
    $('#max_price').val('')
    $('#country_filters_form').submit()
    false

  $('#country_sort_submit').on 'click', ->
    $('#sort_by').val('')
    $('#country_filters_form').submit()
    false

  $('#country_cat_submit').on 'click', ->
    $('#category').val('');
    $('#country_filters_form').submit()
    false