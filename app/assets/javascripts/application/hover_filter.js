$(document).ready(function () {
  
  if($('.filter-list_category>li').length>0){
    $('.filter-list_category>li').mouseenter(function () {
      $(this).addClass('hover');
      $(this).find('#show_hover_subcategory').addClass('hover');   
    });  

    $('.filter-list_category>li').mouseleave(function(){
      $(this).removeClass('hover');
      $(this).find('#show_hover_subcategory').removeClass('hover');
    }); 
  }
})