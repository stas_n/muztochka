(function() {
  var controlsList;

  controlsList = function() {
    if ($('.control-user').length === 0) {
      return false;
    }
    $('body').on('click', '.control-user', function() {
      if ($('.user-menu').hasClass('visible')) {
        $('.user-menu').removeClass('visible');
      } else {
        $('.user-menu').addClass('visible');
      }
      return false;
    });
    /*$('body').on('click', '.user-menu a', function(e) {
      return e.stopPropagation();
    });*/
    return $('body').on('click', '*:not(.control-user, .control-user *, .user-menu a)', function() {
      if ($('.user-menu').hasClass('visible')) {
        $('.user-menu').removeClass('visible');
      }
      return true;
    });
  };

  $(function() {
    return controlsList();
  });

}).call(this);

$(function () {
  $('.header_search').typed({
      strings: ['Fender Telecaster', 'Gibson Les Paul', 'Tubescreemer', 'Marshall MG15', 'Yamaha DX'],
      typeSpeed: 80,
      loop: true,
      preStringTyped: function () {
          $('.header_search').click(function (event) {
              $(this).val('');
              clearInterval($(this).data('typed').timeout);
              $(this).removeData('typed');
          });
      }
  });
});



$(document).ready(function(){
    $('#unread_msgs').click(function(){
        $.ajax({
            type: 'POST',
            url: '/mark_as_read',
            success: 200

    })  })

});

$(document).ready(function () {
    if ($('*:contains("cвяжитесь с нами")').length > 0) {
        $('*:contains("cвяжитесь с нами")').last().attr('id', 'feedback_button_alt')
    }

});
