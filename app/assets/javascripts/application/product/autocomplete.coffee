$ ->
  if gon?
    if gon.brands?
      $("#brand").autocomplete
        source: gon.brands
        messages:
          noResults: '',
          results: ->
            ''
    if gon.producing_countries?
      $("#country_producing").autocomplete
        source: gon.producing_countries
        messages:
          noResults: '',
          results: ->
            ''
