$ ->

  $('#main_submit').on 'click', ->
    $('#main_filter').val('')
    $('#filter_form').submit()
    false

  $('#subcat_submit').on 'click', ->
    $('#subcategory').val('')
    $('#filter_form').submit()
    false

  $('#detail_submit').on 'click', ->
    $('#detail').val('')
    $('#filter_form').submit()
    false

  $('#max_year_submit').on 'click', ->
    $('#max_year').val('')
    $('#filter_form').submit()
    false

  $('#min_year_submit').on 'click', ->
    $('#min_year').val('')
    $('#filter_form').submit()
    false

  $('.collect__form').on 'change', ->
    $('#filter_form').submit()

  $('body').on 'click', '.brand_params a', ->
    brand = $(this).attr('brand-id')
    $('#checkbox_' + brand).trigger 'click'
    false

