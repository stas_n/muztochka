$ ->
  $('#brand_filters_list a').on 'click', ->
    $('#category').val($(this).attr('data_id'))
    $('#subcategory').val($(this).attr('sub_data_id'))
    $('#detail').val($(this).attr('det_data_id'))
    $('#brands_filters_form').submit()
    false




  $('#brand_list_years a').on 'click', ->
    $('#min_year').val($(this).attr('data-min'))
    $('#max_year').val($(this).attr('data-max'))
    $('#brands_filters_form').submit()
    false


  $('#brands_filtering__buttons a').on 'click', ->
    $('#main_filter').val($(@).attr('data-type'))
    $('#brands_filters_form').submit()
    false

  $('#submit_filter_form').on 'click', ->
    $(@).closest('form').submit()
    false
