$ ->
  $('#city_filters_list a').on 'click', ->
    $('#category').val($(this).attr('data_id'))
    $('#subcategory').val($(this).attr('sub_data_id'))
    $('#detail').val($(this).attr('det_data_id'))
    $('#city_filters_form').submit()
    false

  $('#city_list_years a').on 'click', ->
    $('#min_year').val($(this).attr('data-min'))
    $('#max_year').val($(this).attr('data-max'))
    $('#city_filters_form').submit()
    false

  $('#city_filtering__buttons a').on 'click', ->
    $('#main_filter').val($(@).attr('data-type'))
    $('#city_filters_form').submit()
    false

  $('#submit_city_filter_form').on 'click', ->
    $('#city_filters_form').submit()
    false

  $('.check_city_form').on 'change', ->
    $('#city_filters_form').submit()


  $('#city_main_submit').on 'click', ->
    $('#main_filter').val('')
    $('#city_filters_form').submit()
    false

  $('#city_cat_submit').on 'click', ->
    $('#category').val('')
    $('#city_filters_form').submit()
    false

  $('#city_subcat_submit').on 'click', ->
    $('#subcategory').val('')
    $('#city_filters_form').submit()
    false

  $('#city_detail_submit').on 'click', ->
    $('#detail').val('')
    $('#city_filters_form').submit()
    false

  $('#city_max_year_submit').on 'click', ->
    $('#max_year').val('')
    $('#city_filters_form').submit()
    false

  $('#city_min_year_submit').on 'click', ->
    $('#min_year').val('')
    $('#city_filters_form').submit()
    false

  $('#city_price_submit').on 'click', ->
    $('#max_price').val('')
    $('#city_filters_form').submit()
    false

  $('#city_sort_submit').on 'click', ->
    $('#sort_by').val('')
    $('#city_filters_form').submit()
    false
