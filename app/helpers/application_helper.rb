module ApplicationHelper
  def goods_sort_type_to_text type
    case type
      when 'all' then
        'Смотреть все'
      when 'non-active' then
        'Смотреть не активные'
      when 'active' then
        'Смотреть активные'
      else
        'Смотреть все'
    end
  end

  def blog_class(title)
    case title
      when 'статья' then
        'article'
      when 'урок' then
        'lesson play'
      when 'обзор' then
        'overview'
      when 'фото' then
        'foto'
    end
  end

  def remove_phone_number(text)
    text.gsub(/\(*[0-9]+(\)|-)+\s*[0-9]+(-|\s)*[0-9]+(-|\s)*[0-9]+|[0-9]{10}/, '').squish
  end

  def blog_active_category
    if params[:articles]
      'Статьи'
    elsif params[:overviews]
      'Обзоры'
    elsif params[:lessons]
      'Уроки'
    elsif params[:photos]
      'Фото'
    else
      'ВЕСЬ ROCK’N’ROLL'
    end
  end

  def main_filter_to_text filter
    case filter
      when 'handmade' then 'Б/У и Handmade'
      when 'new' then 'Новые товары'
    end
  end

  def sort_by_to_text sort
    case sort
      when 'newest' then 'Новые'
      when 'price_low_first' then 'По возрастанию цены'
      when 'price_high_first' then 'По убыванию цены'
      else ''
    end
  end

  def year_filter_to_text min=nil, max=nil
    if max.blank? and min.blank?
      return ''
    elsif min.blank?
      return "по #{max}"
    elsif max.blank?
      return "с #{min}"
    else
      return "#{min} - #{max}"
    end
  end

  def brands_ids_to_text ids=[]
    if ids
      @brands_arr = ids.map do |id|
        Brand.find(id).title
      end
      return @brands_arr.join(',')
    end
    "<div class='placeholder'>Выберите бренды</div>"
  end

  def pluralize_products(count)
    count.to_s << ' ' << Russian.p(count, 'объявление', 'объявления', 'объявлений')
  end



  def currency_sign
    currency = cookies[:user_currency]
    case currency
      when 'usd' then
        '$'
      when 'eur' then
        '€'
      when 'uah' then
        '₴'
      when 'rub' then
        '₽'
      else
        '$'
    end
  end

  def price_format(price)
     format = price.to_i >= 1 ? number_to_currency(price,precision: 0) : number_to_currency(price)
     currency_sign+' '+format
  end

  def price(price)
    return price_format(price) if cookies[:user_currency] == 'usd'
    to =  cookies[:user_currency]
    user_price =  send("usd_to_#{to}", price.to_f) if to.present?  #CurrencyExchange module meth
    price_format(user_price)
  end

  def user_currency
    currency_sign+' '+ cookies[:user_currency].upcase
  end

  def currency(currency)
    cookies[:user_currency] == currency ? 'active_currency' : "currency_#{currency} currency"
  end

end
