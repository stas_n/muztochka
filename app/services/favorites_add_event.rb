class FavoritesAddEvent

  def self.notify_user fav_id, time=DateTime.now
    user_id = Favorite.find(fav_id).user.id
    EmailTriggerWorker.perform_in(time, user_id, :favorites, fav_id)
  end

  true

end
