class MessageReceivedEvent

  def self.notify_user conversation_id, sender_id, recipient_id, time=DateTime.now
    conversation = Conversation.find(conversation_id)
    if conversation.messages.count >= 1
      EmailTriggerWorker.perform_in(time, recipient_id, :message_received, nil, sender_id, conversation_id)
    end
  end

end
