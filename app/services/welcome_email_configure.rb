class WelcomeEmailConfigure

  def self.notify_user user_id, time=DateTime.now
    EmailTriggerWorker.perform_in(time, user_id, :configure)
  end

  true

end
