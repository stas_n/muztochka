class MessageSentEvent

  def self.notify_user conversation_id, sender_id, recipient_id, time=DateTime.now
    conversation = Conversation.find(conversation_id)
    unless conversation.messages.count > 1
      EmailTriggerWorker.perform_in(time, sender_id, :message_sent, nil, recipient_id)
    end
  end

  true

end
