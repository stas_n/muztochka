
module Scopable
  extend ActiveSupport::Concern

  included do

    scope 'active', -> { where(permission: self.permissions[:active]) }
    scope 'non_active', -> { where(permission: self.permissions[:non_active]) }
    scope 'deleted', -> { where(permission: self.permissions[:deleted]) }
    scope 'of_owner', -> { where(permission: [self.permissions[:active], self.permissions[:non_active]]) }

    default_scope { where(permission: self.permissions[:active]) }
    default_scope { order('created_at DESC') }

    scope "Электрогитары", -> { where(:category_id => 1) }
    scope "Педали и эффекты", -> { where(:category_id => 2) }
    scope "Усилители", -> { where(:category_id => 3) }
    scope "Акустические гитары", -> { where(:category_id => 4) }
    scope "Аксессуары и разное", -> { where(:category_id => 5) }
    scope "Бас-гитары", -> { where(:category_id => 6) }
    scope "Ударные и перкуссия", -> { where(:category_id => 7) }
    scope "Духовые", -> { where(:category_id => 8) }
    scope "Смычковые", -> { where(:category_id => 9) }
    scope "Щипковые", -> { where(:category_id => 10) }
    scope "Народные инструменты", -> { where(:category_id => 11) }
    scope "Клавишные", -> { where(:category_id => 12) }
    scope "DJ оборудование", -> { where(:category_id => 13) }
    scope "Звук", -> { where(:category_id => 14) }
    scope "Свет и шоу", -> { where(:category_id => 15) }
    scope "Другое", -> { where(:category_id => 16) }

    scope :by_search,      -> (search) { where("title || mark ILike ?", "%#{search}%") if search.present? }
    scope :by_category,    -> (category) { where(category_id: category) if category.present? }
    scope :by_subcategory, -> (subcategory) { where(subcategory_id: subcategory) if subcategory.present? }
    scope :by_detail,      -> (detail) { where(detail_id: detail) if detail.present? }
    scope :by_brands,      -> (brands) { where(brand_id: brands) if brands.present? }
    scope :by_min_year,    -> (min_year) { where('year >= ?', min_year.to_i) if min_year.present? }
    scope :by_max_year,    -> (max_year) { where('year <= ?', max_year.to_i) if max_year.present? }

    scope :by_main_filter, -> (main_filter) do
      if main_filter == "handmade"
        where('condition != ? OR handmade = ?', "Новое", true)
      elsif main_filter == "new"
        where(condition: "Новое")
      end
    end

    scope :by_sort, -> (sort) do
      if sort == 'newest'
        reorder('created_at DESC')
      elsif sort == 'price_low_first'
        reorder('price ASC')
      elsif sort == 'price_high_first'
        reorder('price DESC')
      end
    end

    scope :by_max_price, -> (price) { where('price <= ?', price) if price.present? }
    scope :by_type, -> (type) do
      if type == 'non-active'
        non_active
      elsif type == 'active'
        active
      end
    end
  end
end
