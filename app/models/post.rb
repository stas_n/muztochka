class Post < ActiveRecord::Base
  default_scope { order('published_at DESC') }

  validates :title,:author,:category,
            :first_paragraph_text,
            presence: true

  has_one :photo, dependent: :destroy
  has_many :post_photos, dependent: :destroy,class_name: 'PostsPhoto'
  accepts_nested_attributes_for :photo, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :post_photos, allow_destroy: true, reject_if: :all_blank
  validate :check_main_photo

  before_save :only_one_main

  include Viewable
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged
  after_create :remake_slug

  def remake_slug
    self.update_attribute(:slug, nil)
    self.save!
  end

  def slug_candidates
    [
        :title,
        [:title, :id],
    ]
  end

  def self.categories
    %w(статья урок обзор фото)
  end

  def check_main_photo
    errors.add(:photos, 'Добавьте фото для заголовка') if self.photo.blank?
  end

  def check_slider_photo
    errors.add(:post_photos, 'Добавьте фото для слайдера') if self.post_photos.size <= 0
  end

  def enable!
    self.update_attribute(:important,true)
  end

  def self.important_post
    find_by(important:true)
  end


  def only_one_main
    if self.important
      other = self.class.where('important=? and id !=?', true,self.id).first
      if other.present?
        other.update_attribute(:important, false)
      end
    end
  end

end
