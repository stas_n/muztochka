class Subcategory < ActiveRecord::Base
  before_save :set_seo

  belongs_to :category
  has_many :products

  validates :category, presence: true

  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, :presence => true

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

  has_attached_file :image, :styles => { :large => "1920x220#", :thumb => "284x44#" },
                            default_style: :original, :default_url =>  "bg/bg_filter.jpg"
  validates_attachment :image,
                        content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                        size: { in: 0..5.megabytes }

  default_scope { order('position') }

  def set_seo
    if self.seo_title.blank?
      self.seo_title = self.title + " - купить " + self.title.downcase + ">Супер цена по Украине и в Киеве"
    end

    if self.seo_description.blank?
      self.seo_description = "♯ Большой выбор " + self.title.downcase + " на Mуzточка ♯ Купить " + self.title.downcase + " по выгодной цене в Киеве и Украине.✓"
    end
  end
end
