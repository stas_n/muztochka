class Product < ActiveRecord::Base
  before_save :check_city
  before_save :set_seo

  acts_as_punchable

  include Viewable
  extend FriendlyId
  include Scopable

  friendly_id :slug_candidates, use: :slugged
  after_create :remake_slug

  validates :title, :description, :condition, :mark, :price, :country, :city, presence: true
  validates :price, :numericality => { :greater_than => 0 }
  validates :video, format: { with: /\A(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})\z/, allow_blank: true }
  validates :brand, :category, presence: true
  validates :subcategory, presence: true, if: Proc.new { |product| product.detail.blank? && product.category.id != 16 }
  validates :detail, presence: true, if: Proc.new { |product| product.category.id != 16 && product.subcategory.blank? }

  validate :validate_photo_error

  belongs_to :category
  belongs_to :subcategory
  counter_culture :subcategory
  belongs_to :detail
  counter_culture :detail
  belongs_to :brand
  belongs_to :shop

  has_many :favorites, dependent: :destroy

  has_many :photos, dependent: :destroy
  accepts_nested_attributes_for :photos, allow_destroy: true

  has_many :promo_items, dependent: :destroy
  has_many :visitors_count, class_name: 'Visitor::Product'
  has_many :deletion_logs, class_name: 'ProductDeletionLog'

  enum permission: %w( active non_active deleted )


  attr_accessor :photo_error

  def validate_photo_error
    errors.add(:photos, "Слишком мало") if self.photo_error
  end

  def validate_photos(arr)
    self.photo_error = true if arr.blank? or arr.length > 16
  end

  def get_photos(arr)
    self.photos.each do |photo|
      photo.update_attributes(:product_id => nil)
    end
    unless arr.blank?
      arr.each do |el|
        Photo.find(el[1]['id']).update_attributes(:product_id => id, :position => el[1]['position'])
      end
    end
  end

  def self.get_conditions
     ['Новое', 'Как новое', 'Отличное', 'Очень хорошее', 'Хорошее', 'Удовлетворительное', 'Плохое', 'Нерабочее']
  end

  def self.get_subcategories
    Subcategory.all.map { |i| [ "#{i.title} (#{i.category.title})", "#{i.id}"] }.sort
  end

  def self.get_details
    Detail.all.map { |i| [ "#{i.title} (#{i.category.title})", "#{i.id}"] }.sort
  end

  def self.search(query)
    # where(:title, query) -> This would return an exact match of the query
    where("title || mark ILike ?", "%#{query}%")
  end

  def self.q(query)
    where("title || mark ILike ?", "%#{query}%")
  end

  def initial
    return '?' if city.blank?
    city.slice(0).chr.upcase
  end

  def self.alpha(query)
    where("city ILike ?", "#{query}%")
  end

  def promo_check
    unless promo_items.payed.active.blank?
      promo_items.payed.active.order('created_at ASC').first.decrease_count
    end
  end

  def set_seo
    if self.seo_title.blank?
      if self.subcategory.blank?
        self.seo_title = self.category.title + " " + (self.detail.try(:title) || '') + " " + self.title + " купить на muztochka.com"
      else
        self.seo_title = self.category.title + " " + self.subcategory.title + " " + self.title + " купить на muztochka.com"
      end
    end

    if self.seo_description.blank?

      if self.subcategory.blank?
        self.seo_description = self.category.title + " " + (self.detail.try(:title) || '') + " " + self.title + ". Всего " + self.price.to_s + "$, " + self.condition + " состояние. " + "Muztochka.com - свяжись с продавцом прямо сейчас"
      else
        self.seo_description = self.category.title + " " + self.subcategory.title + " " + self.title + ". Всего " + self.price.to_s + "$, " + self.condition + " состояние. " + "Muztochka.com - свяжись с продавцом прямо сейчас"
      end
    end
  end

  def check_city
    if Country.find_by(:name => self.country).blank?
      country = Country.create(name: self.country)
    end

    if City.find_by(:name => self.city, :state => self.state).blank?
      if country
        City.create(name: self.city, state: self.state)
      else
        City.create(name: self.city, state: self.state, country_id: Country.find_by(name: self.country).id)
      end
    end
  end

  def count_of_visits
    visitors_count.count.to_s || 0
  end

  def active?
    self.permission == 'active' || self.permission == 'non_active'
  end

  def deleted?
    self.permission == 'deleted'
  end

  def remake_slug
    self.update_attribute(:slug, nil)
    self.save!
  end

  def slug_candidates
    [
        :title,
        [:title, :id],
    ]
  end

  def self.titles
    last(15).map(&:title).shuffle
  end

  def self.in_list?(min, max)
    if min == '>'
      where('year >= ?', max).present?
    elsif min == '<'
      where('year <= ?', max).present?
    else
      where(year: min..max).present?
    end
  end

end
