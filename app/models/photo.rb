class Photo < ActiveRecord::Base
  belongs_to :product
  belongs_to :post

  default_scope { order('position ASC') }

  has_attached_file :image, :styles => {fotorama: '620x',fotorama_thumb:'80x80#',:large => '220x',
                                        :promo => '220x220#', :medium => '110x110#',
                                        :thumb => '180x140#', :similar => '140x140#',
                                        :blog_article_main => '940x400!',
                                        :blog_read_also => '300x180#'},
                    source_file_options: {all: '-auto-orient'},
                    convert_options: {fotorama_thumb:'-quality 75 -strip',fotorama: '-quality 75 -strip',
                                      large:'-quality 50 -strip'}
                    # :processors => [:thumbnail, :paperclip_optimizer]


  validates_attachment :image,
                       content_type: {content_type: /^image\/(jpeg|jpg|png|gif)$/},
                       size: {in: 0..5.megabytes}

end
