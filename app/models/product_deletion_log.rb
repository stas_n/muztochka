class ProductDeletionLog < ActiveRecord::Base
  belongs_to :user
  belongs_to :reason, class_name: 'ProductDeletionReason'
  belongs_to :product

  validates :user, presence: true
  validates :reason, presence: true
  validates :product, presence: true
end
