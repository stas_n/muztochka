class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= AdminUser.new
    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :moderator
      can :manage, Post
    else
      cannot :manage, ActiveAdmin::Page
    end
  end
end
