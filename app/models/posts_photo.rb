class PostsPhoto < ActiveRecord::Base
  belongs_to :post
  has_attached_file :image, :styles => {:large => "220x", :promo => "220x220#", :medium => "110x110#",
                                        :thumb => "180x140#", :similar => "140x140#",:blog_slider => "620x400#"  },
                   source_file_options: { all: '-auto-orient' },
                   convert_options:{ original: '-quality 75 -strip' }


  validates_attachment :image,
                       content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                       size: { in: 0..5.megabytes }

end
