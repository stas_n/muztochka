class Message < ActiveRecord::Base
  acts_as_readable :on => :created_at
  # default_scope { order('created_at ASC') }

  belongs_to :sender, class_name: 'User'
  belongs_to :conversation, touch: true

  validates :sender, :conversation, presence: true

  after_create :send_email_notification

  def date_string
    self.created_at.strftime('%Y-%m-%d')
  end

  def unreaded?(user)
    user != sender && self.conversation.recipient_read_at < self.created_at
  end

  private

  def send_email_notification
   MessageReceivedEvent.notify_user self.conversation_id, self.sender_id, self.recipient_id
   MessageSentEvent.notify_user self.conversation_id, self.sender_id, self.recipient_id
  end

  def self.unread_messages(user)
    unread_by(user).where(recipient_id:user.id)
  end

end
