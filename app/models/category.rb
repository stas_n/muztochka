class Category < ActiveRecord::Base
  before_save :set_seo

  has_many :subcategories, dependent: :destroy
  has_many :details, dependent: :destroy
  has_many :products, dependent: :destroy

  accepts_nested_attributes_for :subcategories, allow_destroy: true
  accepts_nested_attributes_for :details, allow_destroy: true

  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, :presence => true

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

  has_attached_file :image, :styles => { :large => "1920x220#", :thumb => "284x44#" },
                            default_style: :original, :default_url =>  "bg/bg_filter.jpg"
  validates_attachment :image,
                        content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                        size: { in: 0..5.megabytes }

  default_scope { order('position') }

  def set_seo
    if self.seo_title.blank?
      self.seo_title = self.title + " купить по цене от 50$ на muztochka.com"
    end

    if self.seo_description.blank?
      self.seo_description = "Хочешь купить " + self.title + "?" + " " + "Покупай на Музточке: б/у и новые " + self.title + ". Muztochka.com - товары напрямую от продавцов. Выгодные цены. Удобный сервис."
    end
  end

  def iconic
    case self.id
      when 1
        "icon/icons_0015_electric.png"
      when 2
        "icon/icons_0014_pedals.png"
      when 3
        "icon/icons_0013_amps.png"
      when 4
        "icon/icons_0012_acoustic.png"
      when 5
        "icon/icons_0011_acsessories.png"
      when 6
        "icon/icons_0010_bass.png"
      when 7
        "icon/icons_0009_drums.png"
      when 8
        "icon/icons_0008_wind.png"
      when 9
        "icon/icons_0007_orchestra.png"
      when 10
        "icon/icons_0006_harp.png"
      when 11
        "icon/icons_0005_folk.png"
      when 12
        "icon/icons_0004_keys.png"
      when 13
        "icon/icons_0003_dj.png"
      when 14
        "icon/icons_0002_sound.png"
      when 15
        "icon/icons_0001_lights.png"
    end
  end

end
