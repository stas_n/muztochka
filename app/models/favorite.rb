class Favorite < ActiveRecord::Base

  belongs_to :user
  belongs_to :product

  after_create { FavoritesAddEvent.notify_user self.id }

end
