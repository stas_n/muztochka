ActiveAdmin.register Brand do
  menu priority: 4, :if => proc{ current_admin_user.has_role?(:admin) }

  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  permit_params :title, :approved, :slug, :text, :image,
                :seo_title, :seo_description, :seo_keywords
  config.sort_order = 'created_at_desc'

  index download_links: false do
    column :title
    column :approved
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :approved
    end
  end

  form do |f|
    f.inputs do
      f.input :image, image_preview: true
      f.input :title
      f.input :text
      f.input :approved
    end

    f.inputs name: "SEO" do
      f.input :seo_title
      f.input :seo_description, input_html: { rows: 4 }
      f.input :seo_keywords, :input_html => { :rows => 5 }
    end        

    f.actions
  end

end


