ActiveAdmin.register Role do
    menu priority: 2,:if => proc{ current_admin_user.has_role?(:admin) }
  
    permit_params :name
  
    index do
        selectable_column
        column :name
        actions
      end
  
    filter :name
  
  
    form do |f|
        f.inputs "Admin Details" do
          f.input :name
        end
      f.actions
    end


end