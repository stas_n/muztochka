ActiveAdmin.register Content do
  menu priority: 12, :if => proc{ current_admin_user.has_role?(:admin) }


  before_filter do
    Content.class_eval do
      def to_param
        title
      end
    end
  end

  controller do
    defaults finder: :find_by_title
  end
  config.filters = false

  permit_params :title, :description, :seo_title, :seo_description, :header, :seo_keywords

  actions :all, except: [:destroy]

  index download_links: false do
    column :title
    column :header
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :header
      row :description do
        f.description.try :html_safe
      end
      row :seo_title
      row :seo_description
      row :seo_keywords
    end
  end

  form do |f|
    f.inputs do
      f.input :title, :input_html => { :disabled => true }
      f.input :header
      f.input :description, as: :ckeditor
    end

    f.inputs do
      f.input :seo_title
      f.input :seo_description, input_html: { rows: 4 }
      f.input :seo_keywords, input_html: { rows: 5 }
    end

    f.actions
  end

end
