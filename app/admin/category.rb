ActiveAdmin.register Category do
  menu priority: 3, :if => proc{ current_admin_user.has_role?(:admin) }

  config.filters = false

  permit_params :title, :position, :slug, :text, :seo_text, :image,
                :seo_title, :seo_description, :seo_keywords,
                :subcategories_attributes => [:title, :position, :id, :_destroy, :category_id],
                :details_attributes => [:title, :position, :id, :_destroy, :category_id]
  actions :all, except: [:destroy]
  config.filters = false
  config.sort_order = 'position'


  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  index download_links: false do
    column :title
    column :position
    actions
  end

  show do |f|
    attributes_table do
      row :title
    end
    panel 'Субкатегории подробнее' do
      table_for category.subcategories do
        column do |sub|
          sub.title
        end
        column do |sub|
          sub.position
        end
      end
    end
    panel 'Детали и принадлежности' do
      table_for category.details do
        column do |detail|
          detail.title
        end
        column do |detail|
          detail.position
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :image, image_preview: true
      f.input :title
      f.input :text
      f.input :seo_text
      f.input :position
    end

    f.inputs name: "SEO" do
      f.input :seo_title
      f.input :seo_description, input_html: { rows: 4 }
      f.input :seo_keywords, input_html: { rows: 5 }
    end

    f.inputs name: "Субкатегории" do
      f.has_many :subcategories, :allow_destroy => true, :new_record => true do |cf|
        cf.input :title
        cf.input :position
      end
    end

    f.inputs name: "Детали и принадлежности" do
      f.has_many :details, :allow_destroy => true, :new_record => true do |cf|
        cf.input :title
        cf.input :position
      end
    end

    f.actions
  end

end
