ActiveAdmin.register AdminUser do
  menu priority: 1,:if => proc{ current_admin_user.has_role?(:admin) }

  permit_params :email, :password, :password_confirmation, role_ids: []

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column :roles do |user|
      user.roles.collect { |c| c.name.capitalize }.to_sentence
    end
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :roles,  as: :check_boxes,:collection => Role.global,
              :label_method => lambda { |el| t "simple_form.options.user.roles.#{el.name}" }
    end
    f.actions
  end
# https://github.com/activeadmin/activeadmin/wiki/Keep-AdminUser-logged-in-after-updating-own-password
  controller do
    def update
      current_id = current_admin_user.id
      @admin_user = AdminUser.find(params[:id])
      if @admin_user.update_attributes(permitted_params[:admin_user])
        if @admin_user.id == current_id
          sign_in(AdminUser.find(current_id), :bypass => true)
        end
        flash[:notice] = I18n.t('devise.passwords.updated_not_active')
        redirect_to '/admin/admin_users'
      else
        render :action => :edit
      end
    end
  end
end
