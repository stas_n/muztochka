ActiveAdmin.register Country do
  menu priority: 15,:if => proc{ current_admin_user.has_role?(:admin) }

  config.filters = false
  config.sort_order = 'name_asc'

  permit_params :name, :text, :permalink, :seo_title, :seo_description,
                :seo_text, :image,:seo_keywords

  actions :all, except: [:destroy]

  index download_links: false do
    column :name
    column :permalink
    column :created_at
    actions
  end

  show do |f|
    attributes_table do
      row :name
      row :permalink
      row :image do |country|
        image_tag country.image.url(:thumb)
      end
    end
    attributes_table do
      row :text
      row :seo_text
    end
    attributes_table do
      row :seo_title
      row :seo_description
      row :seo_keywords
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :text, :input_html => { :rows => 5 }
      f.input :seo_text, :input_html => { :rows => 5 }
      f.input :image, image_preview: true
    end

    f.inputs do
      f.input :seo_title
      f.input :seo_description, :input_html => { :rows => 5 }
      f.input :seo_keywords, input_html: { rows: 5 }
    end

    f.actions
  end

end

