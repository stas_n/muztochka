ActiveAdmin.register Banner do
  menu priority: 5,:if => proc{ current_admin_user.has_role?(:admin) }

  config.filters = false

  controller do
    def scoped_collection
      Banner.unscoped
    end
  end

  actions :all, except: [:new, :destroy]

  config.sort_order = 'position_asc'

  permit_params :title, :position, :image, :published, :link,:text

  index download_links: false do
    column :image do |f|
      image_tag f.image.url(:small)
    end
    column :title
    column :text
    column :position
    column :published
    actions
  end

  show do |f|
    attributes_table do
      row :image do
        image_tag f.image.url(:medium)
      end
      row :title
      row :text
      row :position
      row :link
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :text
      f.input :image, image_preview: true
      f.input :link
      f.input :published
    end
    f.actions
  end

end
