ActiveAdmin.register Subcategory do
  menu priority: 4, :if => proc{ current_admin_user.has_role?(:admin) }

  permit_params :title, :position, :slug, :text, :image, :seo_text,
                :category_id, :seo_title, :seo_description, :seo_keywords


  actions :all, except: [:destroy]
  config.sort_order = 'position'

  filter :category

  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  index download_links: false do
    column :title
    column :category
    column :position
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :category
      row :text
      row :image do |sub|
        image_tag sub.image
      end
    end

  end

  form do |f|
    f.inputs do
      f.input :image, image_preview: true
      f.input :category, as: :select
      f.input :title
      f.input :text
      f.input :seo_text
      f.input :position
    end

    f.inputs name: "SEO" do
      f.input :seo_title
      f.input :seo_description, input_html: { rows: 4 }
      f.input :seo_keywords, input_html: { rows: 5 }
    end

    f.actions
  end

end
