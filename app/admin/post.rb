ActiveAdmin.register Post do
  menu priority: 17,:if => proc{ AdminUser.with_any_role(:moderator, :admin) }

  config.filters = true
  config.per_page = 200

  filter :title, label: 'Название'
  filter :author, label: 'Автор'
  filter :article, label: 'Статья'
  filter :category, label: 'Категория'
  filter :important, label: 'На главной'
  filter :category, label: 'Категория'

  permit_params :title,:author,:article, :video,:category,:important,
                :first_paragraph_title,:first_paragraph_text,
                :second_paragraph_title,:second_paragraph_text,:published_at,
                :photo_attributes => [:image, :position, :_destroy, :id],
                :post_photos_attributes => [:image, :_destroy, :id]


  actions :all


  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  member_action :enable, :method => :put do
    post = Post.find(params[:id])
    post.enable!
    flash[:notice] = "Some Thing has been enabled!"
    redirect_to :action => :index
  end

  index do
    column do |post|
      if post.important?
        'На главной'
      else
        link_to 'На главную', url_for(:action => :enable, :id => post.id), :method => :put
      end
    end

    column :title
    column :author
    column (:first_paragraph_title){ |post|  raw post.first_paragraph_title  }
    column (:first_paragraph_text){ |post| truncate(raw(post.first_paragraph_text),length:100, escape: false) }
    column (:second_paragraph_title){ |post| raw post.second_paragraph_title  }
    column (:second_paragraph_text){ |post|  truncate(raw(post.second_paragraph_text),length:100, escape: false)}

    column (:article) { |post| truncate(raw(post.article),length:100, escape: false) }
    column :category
    column :published_at
    # column :video
    # column :created_at
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :author
      row (:first_paragraph_title){ |post| raw(post.first_paragraph_title) }
      row (:first_paragraph_text){ |post| raw(post.first_paragraph_text) }
      row (:second_paragraph_title){ |post| raw(post.second_paragraph_title) }
      row (:second_paragraph_text){ |post| raw(post.second_paragraph_text) }
      row (:article) { |post| raw(post.article) }
      row :video
      row :category
      row :published_at


        row (:image) {image_tag f.photo.image.url(:large)}

      f.post_photos.order('created_at').each do |photo|
        row :image do
          image_tag photo.image.url(:large)
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :first_paragraph_title,  as: :text
      f.input :first_paragraph_text, input_html: { :class => 'ckeditor' }
      f.input :second_paragraph_title, input_html: { :class => 'ckeditor' }
      f.input :second_paragraph_text, input_html: { :class => 'ckeditor' }
      f.input :article, input_html: { :class => 'ckeditor' }
      f.input :author
      f.input :video
      f.input :category, as: :select, collection:  Post.categories
      f.input :important,  as: :boolean
      f.input :published_at,   as: :date_picker

    end

    f.inputs name: 'Фото на заголовок' do
      f.semantic_errors :photos
      f.has_many :photo,class:'dimension_main',heading: 'Основное', :allow_destroy => true, :new_record => true do |ph|
        ph.input :image, image_preview: true
      end
    end
    f.inputs name: 'Фото для слайдера' do
      f.semantic_errors :post_photos
      f.has_many :post_photos,class:'dimension_slider', heading: 'Cлайдер', :allow_destroy => true, :new_record => true do |ph|
        ph.input :image,image_preview: true
      end
    end
    f.actions
  end

end
