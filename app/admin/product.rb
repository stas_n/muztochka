ActiveAdmin.register Product do
  menu priority: 6,:if => proc{ current_admin_user.has_role?(:admin) }

  config.filters = true
  config.per_page = 200

  filter :title
  filter :category
  filter :subcategory, as: :select, collection: proc {Product.get_subcategories}
  filter :brand
  filter :country
  filter :city
  filter :created_at, label: 'Добавлен'

  permit_params :title, :description, :mark, :year, :color, :price, :condition, :country_producing,
                :country, :city, :state, :handmade, :status, :video,
                :category_id, :subcategory_id, :detail_id, :brand_id, :photos_attributes => [:image, :position, :_destroy, :id]

  actions :all, except: [:new]

  scope :all, default: true
  scope "Электрогитары"
  scope "Педали и эффекты"
  scope "Усилители"
  scope "Акустические гитары"
  scope "Аксессуары и разное"
  scope "Бас-гитары"
  scope "Ударные и перкуссия"
  scope "Духовые"
  scope "Смычковые"
  scope "Щипковые"
  scope "Народные инструменты"
  scope "DJ оборудование"
  scope "Звук"
  scope "Свет и шоу"
  scope "Другое"

  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  index download_links: false do
    column :title
    column :subcategory
    column :category
    column :brand
    column :created_at
    column :country
    column :city
    actions
  end

  show do |f|
    attributes_table do
      row "Пользователь" do |product|
        product.shop.user
      end
      row :brand
      row :category
      row :subcategory if product.subcategory
      row :detail
      row :title
      row :mark
      row :description
      row :condition
      row :price

      row :formatted_address
      row :country
      row :state
      row :city
      row :country_producing

      f.photos.order('position').each do |photo|
        row :image do
          image_tag photo.image.url(:large)
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :brand, as: :select, collection: Brand.alpha_order
      f.input :category
      f.input :subcategory
      f.input :detail, as: :select, collection: Product.get_details
      f.input :title
      f.input :mark
      f.input :description
      f.input :color
      f.input :year
      f.input :price, as: :string
      f.input :country_producing
      f.input :handmade
      f.input :condition, as: :select, collection: Product.get_conditions
      f.input :video
      f.input :country, as: :text, input_html: { rows: 1 }
      f.input :city
      f.input :state
      f.input :id, :as => :hidden
    end

    f.inputs name: "Фото" do
      f.has_many :photos, :allow_destroy => true, :new_record => true do |ph|
        ph.input :image, image_preview: true
      end
    end

    f.actions
  end

end
